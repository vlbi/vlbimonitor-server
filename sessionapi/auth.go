package sessionapi

import (
	"net/http"
	"time"
)

// user session exists
func (sc *SessionCache) Auth(r *http.Request) (ok bool) {
	id, err := r.Cookie("sessionid")
	// Delegate request to the given handle
	if err != nil {
		return false
	}

	sc.lock.RLock()
	s, ok := sc.cache[id.Value]
	sc.lock.RUnlock()
	if ok {
		s.LastseenTime = time.Now()
	}
	return ok
}
