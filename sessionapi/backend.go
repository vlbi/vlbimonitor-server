package sessionapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/userapi"
	"database/sql"
	"github.com/lib/pq"
	"log"
	"sync"
	"time"
)

const sql_CreateTable string = `CREATE TABLE IF NOT EXISTS session (
	id TEXT PRIMARY KEY,
	username TEXT NOT NULL,
	logintime INTEGER NOT NULL
)`

// generator
func NewSessionCache(db *sql.DB, users *userapi.UserCache) *SessionCache {
	// make session cache
	sc := SessionCache{
		db:    db,
		cache: make(map[string]*UserSession),
		users: users,
	}

	// init postgres tables
	if _, err := db.Exec(sql_CreateTable); err != nil {
		log.Fatalf("sessionapi: createTable: %v", err)
	}

	// read sessions from database
	sc.loadAll()

	// remove expired sessions from the cache and database
	cleanupWorker := func() {
		for {
			idremove := make([]string, 0, len(sc.cache))
			now := time.Now()

			// drop sessions from the cache
			sc.lock.RLock()
			for id, loginSession := range sc.cache {
				if now.After(loginSession.LastseenTime.Add(expTimeDelta)) {
					idremove = append(idremove, id)
				}
			}
			sc.lock.RUnlock()

			sc.deleteSome(idremove)

			time.Sleep(time.Minute)
		}
	}
	go cleanupWorker()

	return &sc
}

// receiver type
type SessionCache struct {
	db    *sql.DB
	cache map[string]*UserSession
	lock  sync.RWMutex
	users *userapi.UserCache
}
type UserSession struct {
	Id           string `json:"id" bson:"_id"`
	Username     string `json:"-"`
	LoginTime    time.Time
	LastseenTime time.Time `json:"-" bson:"-"`
}

func (sc *SessionCache) deleteSome(idremove []string) {
	if len(idremove) == 0 {
		return
	}

	// drop sessions from cache
	sc.lock.Lock()
	for _, id := range idremove {
		delete(sc.cache, id)
	}
	sc.lock.Unlock()

	// drop sessions from PG
	sql := `DELETE FROM session WHERE id = ANY($1)`
	if _, err := sc.db.Exec(sql, pq.Array(idremove)); err != nil {
		log.Println("deleteSessions: Error removing sessions from PG", err)
	}
}

func (sc *SessionCache) GetSession(sessionId string) *UserSession {
	sc.lock.RLock()
	defer sc.lock.RUnlock()
	s, _ := sc.cache[sessionId]
	return s
}

func (sc *SessionCache) Insert(s *UserSession) {
	// insert in session cache
	sc.lock.Lock()
	sc.cache[s.Id] = s
	sc.lock.Unlock()

	// insert session in PG
	sql := `INSERT INTO session (id, username, logintime) VALUES ($1, $2, $3)`
	if _, err := sc.db.Exec(sql, s.Id, s.Username, s.LoginTime.Unix()); err != nil {
		log.Println("NewSession: Error inserting session in PG:", err)
	}
}

func (sc *SessionCache) loadAll() {
	// load sessions from PG
	sql := `SELECT id, username, logintime FROM session`
	rows, err := sc.db.Query(sql)
	if err != nil {
		log.Println("loadSessions: Error loading sessions from PG:", err)
	}
	defer rows.Close() // good practice, though rows are unconditionally consumed below
	var logintime int64
	var s UserSession
	now := time.Now()
	sc.lock.Lock()
	for rows.Next() {
		rows.Scan(&s.Id, &s.Username, &logintime)
		s.LoginTime = time.Unix(logintime, 0)
		s.LastseenTime = now
		sc.cache[s.Id] = &s
	}
	sc.lock.Unlock()

	if len(sc.cache) > 0 {
		log.Println("User sessions restored:", len(sc.cache))
	}
}
