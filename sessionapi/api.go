package sessionapi

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"time"
)

const (
	tokenLength  = 32
	expTimeDelta = 24 * time.Hour
)

// receiver type
type Api struct {
	*SessionCache
}

// implement the Cruder interface
func (sc *Api) Attached(method string) bool {
	switch method {
	case "create":
		return true
	case "list":
		return false
	case "read":
		return false
	case "update":
		return true
	case "replace":
		return false
	case "delete":
		return true
	}
	return false
}

func (sc *Api) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	user := sc.users.AuthenticateRequest(w, r)
	if user == nil {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	// Generate a new random session id
	bytes := make([]byte, tokenLength)
	if _, err := rand.Read(bytes); err != nil {
		log.Println("SessionId generation error")
		http.Error(w, "SessionId generation error", http.StatusInternalServerError)
		return
	}
	sessionId := base64.URLEncoding.EncodeToString(bytes)

	// compile session
	now := time.Now()
	s := UserSession{sessionId, user.Username, now, now}

	sc.Insert(&s)

	cookie := http.Cookie{Name: "sessionid", Value: sessionId, Path: "/"}
	http.SetCookie(w, &cookie)
	json.NewEncoder(w).Encode(s)

	fmt.Println("Create UserSession:", user.Username, r.RemoteAddr, s.Id)
}

func (sc *Api) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {}

func (sc *Api) Read(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (sc *Api) Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	sc.lock.RLock()
	s, ok := sc.cache[id]
	sc.lock.RUnlock()
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}
	s.LastseenTime = time.Now()

	cookie := http.Cookie{Name: "sessionid", Value: s.Id, Path: "/"}
	http.SetCookie(w, &cookie)
	json.NewEncoder(w).Encode("session updated: " + id)
}

func (sc *Api) Replace(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (sc *Api) Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	sc.lock.RLock()
	_, ok := sc.cache[id]
	sc.lock.RUnlock()
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	ids := []string{id}
	sc.deleteSome(ids)
}
