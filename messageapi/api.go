package messageapi

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	tokenLength = 32
)

// composition
type Api struct {
	*MessageStore
}

// implement the Cruder interface
func (sc *Api) Attached(method string) bool {
	switch method {
	case "create":
		return true
	case "list":
		return true
	case "read":
		return true
	case "update":
		return false
	case "replace":
		return false
	case "delete":
		return true
	}
	return false
}

func (a *Api) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	user := a.users.AuthenticateRequest(w, r)
	if user == nil {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	// decode input
	now := time.Now()
	min := MessageInput{Texttime: now}
	json.NewDecoder(r.Body).Decode(&min)

	// validate input
	// TODO: json schema
	msg := make([]string, 0)
	if len(min.Text) == 0 {
		msg = append(msg, "Text field empty, rejected")
	}
	if len(msg) != 0 {
		http.Error(w, strings.Join(msg, "; "), http.StatusBadRequest)
		return
	}

	message := Message{
		MessageInput: min,
		Recvtime:     now,
		Modtime:      now,
		Username:     user.Username,
	}
	if err := a.AddMessage(&message); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	json.NewEncoder(w).Encode("Id:" + message.Id)
}

func (a *Api) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	// filter parameters
	var twindow [2]time.Time
	for i, par := range []string{"startTime", "endTime"} {
		s, err := strconv.ParseInt(r.FormValue(par), 10, 64)
		if err != nil {
			msg := fmt.Sprintf("value error: %s: %s", par, err.Error())
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		twindow[i] = time.Unix(s, 0)
	}

	marr, err := a.GetMessages(twindow[0], twindow[1])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(marr)
}

func (a *Api) Read(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	m, err := a.GetMessage(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	json.NewEncoder(w).Encode(m)
}

func (a *Api) Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (a *Api) Replace(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (a *Api) Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	if err := a.RemoveMessage(id); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}
