package messageapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/userapi"
	"database/sql"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"html/template"
	"regexp"
	"strings"
	"time"
)

// generator
func NewMessageStore(dbsession *mgo.Session, dbname string, db *sql.DB, users *userapi.UserCache) *MessageStore {
	ms := MessageStore{
		dbsession: dbsession,
		dbname:    dbname,
		db:        db,
		users:     users,
	}
	return &ms
}

// receiver type
type MessageStore struct {
	dbsession *mgo.Session
	dbname    string
	db        *sql.DB
	users     *userapi.UserCache
}
type Message struct {
	Id_ bson.ObjectId `json:"-" bson:"_id"`
	MessageInput
	Recvtime time.Time // original submission time
	Modtime  time.Time // time of last modification
	Userid   string    // foreign key
	Username string
	Tags     []string `json:,omitempty bson:,omitempty` // foreign keys
	Mentions []string `json:,omitempty bson:,omitempty` // foreign keys
}
type MessageInput struct {
	Id         string // primary key
	Text       string
	Texttime   time.Time // user supplied time at which text applied
	References []string  `json:,omitempty bson:,omitempty`
}

func (ms *MessageStore) AddMessage(message *Message) error {
	dbsession := ms.dbsession.Copy()
	defer dbsession.Close()
	cur := dbsession.DB(ms.dbname).C("messages")

	// ObjectId
	message.Id_ = bson.NewObjectId()
	message.Id = message.Id_.Hex()

	// Prevent injection
	message.Text = template.HTMLEscapeString(message.Text)

	// extract tags
	message.Tags, _ = extractTags(message.Text)

	return cur.Insert(*message)
}

func (ms *MessageStore) GetMessage(id string) (*Message, error) {
	dbsession := ms.dbsession.Copy()
	defer dbsession.Close()
	cur := dbsession.DB(ms.dbname).C("messages")

	var m Message
	err := cur.FindId(bson.ObjectIdHex(id)).One(&m)
	return &m, err
}

func (ms *MessageStore) RemoveMessage(id string) error {
	dbsession := ms.dbsession.Copy()
	defer dbsession.Close()
	cur := dbsession.DB(ms.dbname).C("messages")

	return cur.RemoveId(bson.ObjectIdHex(id))
}

func (ms *MessageStore) GetMessages(startTime, endTime time.Time) ([]Message, error) {
	dbsession := ms.dbsession.Copy()
	defer dbsession.Close()
	cur := dbsession.DB(ms.dbname).C("messages")

	marr := make([]Message, 0)
	query := bson.M{"recvtime": bson.M{"$gt": startTime, "$lt": endTime}}
	err := cur.Find(query).Sort("-recvtime").All(&marr)

	return marr, err
}

func contains(e []string, c string) bool {
	for _, s := range e {
		if s == c {
			return true
		}
	}
	return false
}

func unique(e []string) []string {
	r := []string{}

	for _, s := range e {
		if !contains(r[:], s) {
			r = append(r, s)
		}
	}
	return r
}

func extractTags(m string) ([]string, string) {
	re := regexp.MustCompile(`\#(\w+) `)

	match := re.FindAllStringSubmatch(m, -1)

	tags := make([]string, len(match))
	for i, v := range match {
		tags[i] = strings.ToLower(v[1])
	}

	remainder := re.ReplaceAllString(m, "")

	return unique(tags), remainder
}
