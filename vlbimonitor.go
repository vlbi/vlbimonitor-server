// This file is part of the Radboud Radio Lab VLBI Monitor
// Copyright (C) 2016 Pim Schellart
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bitbucket.org/vlbi/vlbimonitor-server/datastore"
	"bitbucket.org/vlbi/vlbimonitor-server/localbackendapi"
	//"bitbucket.org/vlbi/vlbimonitor-server/messageapi"
	//"bitbucket.org/vlbi/vlbimonitor-server/obslogapi"
	"bitbucket.org/vlbi/vlbimonitor-server/replayapi"
	"bitbucket.org/vlbi/vlbimonitor-server/restful"
	"bitbucket.org/vlbi/vlbimonitor-server/sessionapi"
	"bitbucket.org/vlbi/vlbimonitor-server/userapi"
	"bitbucket.org/vlbi/vlbimonitor-server/vexstoreapi"
	"context"
	"database/sql"
	"fmt"
	"github.com/julienschmidt/httprouter"
	_ "github.com/lib/pq"
	"html/template"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

var sessionCache *sessionapi.SessionCache

func handlerFilesWithTemplates(root string, observatories []string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// get the filepath (stolen from http.fileHandler.ServerHTTP())
		upath := r.URL.Path
		if !strings.HasPrefix(upath, "/") {
			upath = "/" + upath
			r.URL.Path = upath
		}
		upath = path.Clean(upath)
		// the combination of prepending the / and Clean makes sure that upath cannot traverse parents outside the root

		// try to open the file
		f, err := os.Open(path.Join(root, upath))
		if err != nil {
			if os.IsNotExist(err) {
				http.Error(w, "404 page not found", http.StatusNotFound)
			}
			if os.IsPermission(err) {
				http.Error(w, "403 Forbidden", http.StatusForbidden)
			}
			http.Error(w, "could not open file", http.StatusInternalServerError)
			return
		}
		defer f.Close()

		// caching for a template should consider the last modtime of all template files and the main file
		d, _ := f.Stat()
		modtime := d.ModTime()
		// modby := upath
		matches, err := filepath.Glob(path.Join(root, "/templates/*.tmpl"))
		if err != nil {
			http.Error(w, "could not list templates", http.StatusInternalServerError)
			return
		}
		for _, file := range matches {
			fmt.Printf("considering template file %s\n", file)
			f2, err := os.Open(file)
			if err != nil {
				http.Error(w, "could not open template", http.StatusInternalServerError)
				return
			}
			defer f2.Close()
			d2, _ := f2.Stat()
			// fmt.Printf("modified at %s\n",d2.ModTime())
			if d2.ModTime().After(modtime) {
				modtime = d2.ModTime()
				// modby = file
			}
		}
		// fmt.Printf("last modified by tempate %s at %s\n",modby,modtime)

		// reply to the client when the files were last modified
		w.Header().Set("Last-Modified", modtime.UTC().Format(http.TimeFormat))

		// done, rangeReq := checkPreconditions(w, r, modtime)
		// ims := r.Header.Get("If-Modified-Since")
		// if ims != "" {
		// 	t, err := http.ParseTime(ims)
		// 	if err == nil && ! t.IsZero() && ! t.Equal(time.Unix(0, 0)) && modtime.Before(t.Add(1*time.Second)) {
		//  				// fmt.Printf("not sending the body because nothing changed since %s\n",t);
		// 		w.WriteHeader(http.StatusNotModified);
		// 		return; // and don't send the body again
		// 	}
		// }

		// parse the main content first and then all templates in the templates dir
		page := template.Must(template.ParseFiles(path.Join(root, upath)))
		template.Must(page.ParseGlob(path.Join(root + "/templates/*.tmpl")))

		// get the username/facility from auth
		templateData := struct {
			UserName      string
			UserFacility  string
			UserLoggedIn  bool
			Observatories []string
			Observatory   string
			Field         string
		}{"", "", false, observatories, r.FormValue("observatory"), r.FormValue("field")}

		sessioncookie, err := r.Cookie("sessionid")
		usernamecookie, _ := r.Cookie("username")
		facilitycookie, _ := r.Cookie("facility")
		if usernamecookie != nil && facilitycookie != nil && sessioncookie != nil {
			templateData.UserName, _ = url.QueryUnescape(usernamecookie.Value)
			templateData.UserFacility, _ = url.QueryUnescape(facilitycookie.Value)
			templateData.UserLoggedIn = err == nil && sessionCache.GetSession(sessioncookie.Value) != nil
		}
		// fmt.Printf("templateData: %+v\n",templateData);
		// attempt to execute the template
		if err := page.Execute(w, templateData); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
}

func cacheControlWrapper(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// log.Printf("handling static file: %s\n",r.URL);
		w.Header().Add("Expires", "Thu, 19 Nov 1981 08:52:00 GM")
		w.Header().Add("Cache-Control", "no-cache")
		h.ServeHTTP(w, r)
	})
}

func main() {
	// Parse command line arguments
	program := os.Args[0]
	if len(os.Args) != 2 {
		log.Fatal("Usage: %s config.yaml", program)
	}
	filename := os.Args[1]
	conf := loadConfigfile(filename)

	// Initialize postgresql session
	var db *sql.DB
	{
		connStr := fmt.Sprintf("user=%s dbname=%s password=%s host=%s port=%d sslmode=disable",
			conf.PostgresInfo.User,
			conf.PostgresInfo.Database,
			conf.PostgresInfo.Pass,
			conf.PostgresInfo.Host,
			conf.PostgresInfo.Port)
		var err error
		if db, err = sql.Open("postgres", connStr); err != nil {
			log.Panic(err)
		}

		// test connection
		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
		if err := db.PingContext(ctx); err != nil {
			log.Fatalf("unable to connect to database: %v", err)
		}
		cancel()
	}

	ds := datastore.NewDataStore(db, conf.RecordSchema, conf.Observatories)

	// Watch Schedules Directory for inotify events
	ds.StartWatching(conf.SchedulesDirectory+"/trigger", 0)

	// setup resources
	userCache := userapi.NewUserCache(db, conf.Admins, conf.AuthorizedProviders)
	replayStore := replayapi.NewReplayStore(userCache, ds.Replayer) // initialize job queue
	sessionCache = sessionapi.NewSessionCache(db, userCache)
	vexstore := vexstoreapi.NewVexstore(conf.SchedulesDirectory)
	//obslogStore := obslogapi.NewObsLogStore(db, userCache, conf.MessageSourceId)
	localBackendStore := localbackendapi.NewLocalBackendStore(conf.LocalObservatory)

	// define authenticators
	authNone := restful.AuthOr(func(r *http.Request) bool { return true })
	authAdmin := restful.AuthOr(userCache.AdminAuth)
	authSession := restful.AuthOr(sessionCache.Auth)
	authSessionUser := restful.AuthOr(sessionCache.Auth, userCache.MinimalAuth)
	authSessionUserLocalhost := restful.AuthOr(sessionCache.Auth, userCache.MinimalAuth, func(r *http.Request) (ok bool) { h, _, _ := net.SplitHostPort(r.Host); return h == "localhost" })

	// router
	router := httprouter.New()

	// install routes
	restful.Attach(router, "/user", &userapi.Api{userCache}, authAdmin)
	restful.Attach(router, "/replay", &replayapi.Api{replayStore}, authAdmin)
	restful.Attach(router, "/session", &sessionapi.Api{sessionCache}, authNone)
	//restful.Attach(router, "/message", &messageapi.Api{messageStore}, authSession)
	//restful.Attach(router, "/obslog", &obslogapi.Api{obslogStore}, authSession)

	// data routes
	router.GET("/data/snapshot", authSession(ds.SnapshotGetHandle))
	router.GET("/data/history", authSessionUserLocalhost(ds.TimeseriesHandle))
	router.GET("/data/historydownload", authSessionUser(ds.TimeseriesDownloadHandle))

	router.POST("/raw/:facility", ds.RawPostHandle(userCache))

	// temporary maintenance endpoints
	router.PATCH("/data/history", authAdmin(ds.TimeseriesPatchHandle))

	router.GET("/vexstore/:path", authSessionUser(vexstore.List))

	router.GET("/local", authSessionUser(localBackendStore.GetHandle))
	router.POST("/local", authSessionUserLocalhost(localBackendStore.PostHandle))

	router.Handler("GET", "/", http.RedirectHandler(conf.LandingPage, http.StatusSeeOther))

	template := cacheControlWrapper(handlerFilesWithTemplates(conf.WebDirectory, conf.Observatories))
	router.Handler("GET", "/login.html", template)
	router.Handler("GET", "/overview.html", template)
	router.Handler("GET", "/paneltest.html", template)
	router.Handler("GET", "/newlayout.html", template)
	router.Handler("GET", "/obslog.html", template)
	router.Handler("GET", "/settings.html", template)
	router.Handler("GET", "/map.html", template)
	router.Handler("GET", "/graph.html", template)
	router.Handler("GET", "/table.html", template)
	router.Handler("GET", "/weather.html", template)
	router.Handler("GET", "/weathermapcolumns.html", template)
	router.Handler("GET", "/celestial.html", template)
	router.Handler("GET", "/matrix.html", template)
	router.Handler("GET", "/messages.html", template)
	router.Handler("GET", "/logs.html", template)
	router.Handler("GET", "/schedule.html", template)
	router.Handler("GET", "/localbackend.html", template)

	// TODO: serve all static content from one fs
	router.ServeFiles("/css/*filepath", neuteredFileSystem{http.Dir(conf.WebDirectory + "/css")})
	router.ServeFiles("/img/*filepath", neuteredFileSystem{http.Dir(conf.WebDirectory + "/img")})
	router.ServeFiles("/js/*filepath", neuteredFileSystem{http.Dir(conf.WebDirectory + "/js")})
	router.ServeFiles("/static/*filepath", neuteredFileSystem{http.Dir(conf.WebDirectory + "/static")})

	server := &http.Server{
		Addr:    conf.ServerAddress,
		Handler: router,
	}
	log.Print("Starting HTTP server")
	log.Fatal(server.ListenAndServe())
}
