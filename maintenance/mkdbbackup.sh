#!/bin/bash

#-- collect nonzero exit stats
ierrs=()

lvcreate -L500M -s -n dbbackup /dev/VolGroupArray/lvhome || { echo "ERROR: lvcreate: $?" >&2; exit 1; }

mount /dev/VolGroupArray/dbbackup /mnt || { echo "ERROR: mount: $?" >&2; exit 1; }

rsync -va /mnt/mongodb/ /backup/mongodb.$(date +%y%m%d)
ierr=$?; [[ $ierr -ne 0 ]] && { echo "ERROR: rsync: $ierr" >&2; ierrs+=($ierr); }

umount /mnt/ || { echo "ERROR: umount: $?" >&2; exit 1; }

lvremove -f /dev/VolGroupArray/dbbackup
ierr=$?; [[ $ierr -ne 0 ]] && { echo "ERROR: lvremove: $ierr" >&2; ierrs+=($ierr); }

#-- count errors
[[ "${#ierrs[@]}" -gt 0 ]] && exit 1
