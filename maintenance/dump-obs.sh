#!/bin/bash
starttime="2017-04-04T22:30:00.000Z"
stoptime="2017-04-11T22:30:00.000Z"
for i in SMTO SMA PICO LMT APEX JCMT; do
	fname="weather_${i}.json"
	echo "dumping ${fname}"
	mongo --quiet vlbi_monitor << EOF |sed -e '1d' > ${fname}
DBQuery.shellBatchSize = 10000000;
db.raw_${i}.find({"header.sendTime": {\$gt: ISODate("${starttime}"), \$lt: ISODate("${stoptime}")}}, {_id: 0, "data.weather:temperature": 1, "data.weather:airPressure": 1, "data.weather:dewPointTemp": 1, "data.weather:relativeHumidity": 1, "data.weather:windSpeed": 1, "data.weather:windDirection": 1});
EOF

	fname="tsys_${i}.json"
	echo "dumping ${fname}"
	mongo --quiet vlbi_monitor << EOF |sed -e '1d' > ${fname}
DBQuery.shellBatchSize = 10000000;
db.raw_${i}.find({"header.sendTime": {\$gt: ISODate("${starttime}"), \$lt: ISODate("${stoptime}")}, "data.if:1:systemTemp": {\$exists: true}}, {_id: 0, "data.if:1:systemTemp": 1, "data.if:2:systemTemp": 1});
EOF
done
