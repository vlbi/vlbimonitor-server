#!/bin/bash
starttime="2016-04-08"
stoptime="2016-04-09"
for i in SMTO JCMT LMT; do
	fname="raw_${i}.json"
	echo "dumping ${fname}"
	mongo --quiet vlbi_monitor << EOF |sed -e '1d' > ${fname}
DBQuery.shellBatchSize = 1000000;
db.raw_${i}.find({"header.sendTime": {\$gt: ISODate("${starttime}"), \$lt: ISODate("${stoptime}")} });
EOF
done
