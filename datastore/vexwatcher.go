package datastore

import (
	"bitbucket.org/vlbi/vlbimonitor-server/vexparser"
	"github.com/fsnotify/fsnotify"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
	"sync"
	"time"
)

type VexfileInfo struct {
	Filename string `json:"filename"`
	Checksum uint32 `json:"checksum"`
	Startvex string `json:"startvex"`
	Stopvex  string `json:"stopvex"`
	ReadTime int64  `json:"readtime"` // support incremental updates
}

type object = vexparser.Object

func (ds DataStore) handleFile(canonical, filename string) bool {
	//log.Println("found file ", filename)
	if !strings.HasSuffix(filename, ".vex") || strings.HasPrefix(filename, ".") {
		return false
	}
	info, err := os.Lstat(canonical)
	if err != nil {
		log.Println("failed to stat file ", filename)
		return false
	}
	if info.IsDir() {
		return false
	}

	// parse vex file
	vex := vexparser.Parse(canonical, filename)
	vex.Expand(vex)
	if err := vex.Sanity(); err != nil {
		log.Printf("vexsanity: %s: %s", filename, err.Error())
		return false
	}

	checksum := vex["checksum"].(uint32)

	// was this file handled before?
	for e := ds.vexfiles.Front(); e != nil; e = e.Next() {
		v := e.Value.(VexfileInfo)
		// support identical vexfiles with different names
		// but not different vexfiles with identical name
		// the old entry is dropped if changes occur
		if v.Filename == filename {
			if v.Checksum == checksum {
				return false
			}
			ds.vexfiles.Remove(e)
		}
	}

	// insert into cache
	v := VexfileInfo{
		Filename: filename,
		Checksum: checksum,
		Startvex: vex["$GLOBAL"].(object)["exper_nominal_start"].(string),
		Stopvex:  vex["$GLOBAL"].(object)["exper_nominal_stop"].(string),
		ReadTime: time.Now().Unix(),
	}
	ds.vexfiles.PushFront(v)
	return true
}

func (ds DataStore) handleFileRemoval(filename string) {
	for e := ds.vexfiles.Front(); e != nil; e = e.Next() {
		v := e.Value.(VexfileInfo)
		if v.Filename == filename {
			log.Println("drop vexfile:", filename)
			ds.vexfiles.Remove(e)
		}
	}
}

// StartWatching uses inotify to see new files appear and disappear and
// schedules a file with a short delay to allow for file I/O operations
// to finish.  The event handler catches inotify errors and restarts the
// watcher if necessary.
func (ds DataStore) StartWatching(dir string, nstart int) {
	if nstart > 0 {
		log.Println("restarting vexfile watcher, attempt", nstart)
	}

	// Append trailing slash
	if dir[len(dir)-1] != '/' {
		dir += "/"
	}
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Println("failed to make a listing of the schedules dir")
	} else {
		for _, f := range files {
			ds.handleFile(dir+f.Name(), f.Name())
		}
	}

	// now start the actual watching part:
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatalf("failed to start a directory watch on dir %v", dir)
	}

	go func() {
		timers := make(map[string]*time.Timer)
		timerLock := sync.Mutex{}
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					log.Printf("fsnotify event channel closed unexpectedly")
					goto Leave
				}

				fname := path.Base(event.Name)
				if !strings.HasSuffix(fname, ".vex") || strings.HasPrefix(fname, ".") {
					continue
				}

				if event.Op&fsnotify.Write == fsnotify.Write ||
					event.Op&fsnotify.Create == fsnotify.Create {
					timerLock.Lock()
					if timer, ok := timers[fname]; ok {
						timer.Reset(time.Second)
					} else {
						//log.Printf("scheduling a new timer for %s", fname)
						timers[fname] = time.AfterFunc(time.Second, func() {
							timerLock.Lock()
							delete(timers, fname)
							timerLock.Unlock()
							if ds.handleFile(event.Name, fname) {
								log.Println("vexfile parsed:", fname)
							}
						})
					}
					timerLock.Unlock()
				} else if event.Op&fsnotify.Rename == fsnotify.Rename ||
					event.Op&fsnotify.Remove == fsnotify.Remove {
					// cancel possibly scheduled re-parsing
					timerLock.Lock()
					if _, ok := timers[fname]; ok {
						timers[fname].Stop()
						delete(timers, fname)
					}
					timerLock.Unlock()
					ds.handleFileRemoval(fname)
				}

			case err, ok := <-watcher.Errors:
				if !ok {
					log.Println("fsnotify error channel closed unexpectedly")
					goto Leave
				}
				log.Println("fsnotify error: ", err)
			}
		}

	Leave:
		if nstart < 10 {
			watcher.Close()
			time.Sleep(10 * time.Second)
			ds.StartWatching(dir, nstart+1)
		}
	}()

	if err := watcher.Add(dir); err != nil {
		log.Println("watcher.Add error:", err)
		watcher.Close()
		if nstart == 0 {
			os.Exit(1)
		}
	}
}
