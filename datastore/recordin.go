package datastore

import (
	"strings"
	"time"
)

type RecordIn struct {
	Id     uint64             `json:"-" bson:",omitempty"`
	Header Header             `json:"header"`
	Data   map[string]DataArr `json:"data,omitempty" bson:",omitempty"`
}

type Header struct {
	RecvTime      time.Time `json:"recvTime,omitempty" bson:"recvTime,omitempty"`
	Username      string    `json:"username,omitempty" bson:",omitempty"`
	Facility      string    `json:"facility,omitempty" bson:",omitempty"`
	ClientVersion string    `json:"clientVersion,omitempty" bson:"clientVersion,omitempty"`
	tDataMin      int64
	tDataMax      int64
}

// Only the last element of each array
func (rec *RecordIn) FlatData() Dataflat {
	data := make(Dataflat)
	for par, dataarr := range rec.Data {
		if len(dataarr) == 0 {
			continue
		}
		par_new := strings.Replace(par, ":", "_", -1)
		data[par_new] = dataarr[len(dataarr)-1]
	}
	return data
}
