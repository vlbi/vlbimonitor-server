package datastore

import (
	"fmt"
	"sync"
	"time"
)

type Cache struct {
	headers   *HeaderArr
	datachain *Chain
	messages  *MsgCache
}

func newCache(obs string) *Cache {
	cache := Cache{}
	cache.headers = &HeaderArr{}
	cache.datachain = newDatachain(obs)
	cache.messages = &MsgCache{}
	return &cache
}

/*
  Message
*/
// avoid using a map to support concurrent access to the cache
var MSGCACHE_FIELDS = [2]string{"status", "weather"}

type MsgCache [2]MessagePoint // see MSGCACHE_FIELDS

func (mc *MsgCache) put(mp *MessagePoint, tag string) {
	for i, f := range MSGCACHE_FIELDS {
		if tag == f {
			mc[i] = *mp
		}
	}
}

func (mc MsgCache) toMap(tfrom int64) map[string]MessagePoint {
	timefrom := time.Unix(tfrom, 0)
	mmap := make(map[string]MessagePoint)
	for i, e := range mc {
		if e.RecvTime.After(timefrom) {
			mmap[MSGCACHE_FIELDS[i]] = e
		}
	}
	return mmap
}

/*
  Header
*/
type HeaderArr [3]Header

func (ha *HeaderArr) put(h *Header) {
	var j, jj int = -1, 0
	for i, e := range *ha {
		// own slot
		if e.Username == h.Username {
			j = i
			break
		}
		// empty slot
		if e.Username == "" {
			j = i
		}
		// oldest slot
		if e.RecvTime.Before(ha[jj].RecvTime) {
			jj = i
		}
	}
	// overwrite oldest
	if j <= -1 {
		j = jj
	}
	//fmt.Println("put:", j, ha[j].Username, h.Username) // DEBUG
	ha[j] = *h
}

func (ha HeaderArr) toMap(tfrom int64) map[string]ClientStat {
	stats := make(map[string]ClientStat)
	timefrom := time.Unix(tfrom, 0)
	for _, e := range ha {
		if e.Username == "" {
			continue
		}
		if e.RecvTime.Before(timefrom) {
			continue
		}
		stats[e.Username] = ClientStat{e.RecvTime.Unix(), e.ClientVersion}
	}
	return stats
}

/*
  Data
*/
type Dataflat map[string][2]interface{}

type Node struct {
	id   int   // node counter, for info only
	time int64 // unix time, compatible with time.Unix()
	data Dataflat
	next *Node
}

type Chain struct {
	name  string // for info only
	mutex sync.Mutex
	head  *Node
	tail  *Node
}

// append new node to the chain
func (chain *Chain) push(data Dataflat) {
	now := time.Now().Unix()

	// mutex
	chain.mutex.Lock()
	node := &Node{id: chain.head.id + 1, time: now, data: data}
	chain.head.next = node
	chain.head = node
	chain.mutex.Unlock()

	if chain.name == "SOMESITE" {
		fmt.Println("grow head:", chain.name, node.id) // DEBUG
	}
}

// return cumulative update since tfrom
func (chain Chain) get(tfrom int64) Dataflat {
	data := make(Dataflat)

	for n := chain.tail; n != nil; n = n.next {
		// skip pre-tfrom
		if n.time < tfrom {
			continue
		}
		for k, v := range n.data {
			// don't update if data is historic
			if w, ok := data[k]; !ok || v[0].(float64) > w[0].(float64) {
				data[k] = v
			}
		}
	}
	return data
}

// return new chain from which inserted nodes get dropped from the tail or merged to deresolve
func newDatachain(name string) *Chain {
	const AGELIMIT = 660 // seconds
	const DT_OVER_T = .5

	// initialize with empty map to make it iterable
	datamap := make(Dataflat)
	head := Node{time: time.Now().Unix(), data: datamap}
	chain := Chain{name: name, head: &head, tail: &head}

	maintenance := func() (int, int) {
		var t, dt, dt_tg, tnow int64
		tnow = time.Now().Unix()
		var length int = 1 // the head
		var ndropped int = 0
		for n := chain.tail; n.next != nil; n = n.next {
			length += 1

			// We need at least two more nodes in order to be able to merge one
			if n.next.next == nil {
				break
			}

			// distance to over-next node too big to absorpt next node?
			t = tnow - n.time
			dt_tg = int64(float64(t) * DT_OVER_T)
			dt = n.next.next.time - n.time
			if dt >= dt_tg {
				continue
			}
			// node next to tail is not old enough
			if n.id == 0 && tnow-n.next.time < AGELIMIT {
				continue
			}

			// absorb next node
			dataflat := make(Dataflat)
			for k, v := range n.data {
				dataflat[k] = v
			}
			for k, v := range n.next.data {
				dataflat[k] = v
			}
			if chain.name == "SOMESITE" {
				fmt.Printf("merge node %d into %d: %s\n", n.next.id, n.id, chain.name) // DEBUG
			}
			n.data = dataflat
			nn := n.next
			n.next = n.next.next
			nn.next = nil // overy careful to avoid memory leaks
			ndropped += 1
		}
		return ndropped, length
	}

	scheduler := func() {
		var interval int = 10
		for {
			time.Sleep(time.Duration(interval) * time.Second)
			nremoved, n := maintenance()
			if chain.name == "SOMESITE" {
				var ids []int
				for n := chain.tail; n != nil; n = n.next {
					ids = append(ids, n.id)
				}
				fmt.Printf("cache maint: nremoved: %d, length: %d, interval: %d, name: %s, ids: %v\n", nremoved, n, interval, chain.name, ids) // DEBUG
			}

			// target 3 removals per cycle
			if nremoved == 0 {
				interval = interval * 2
			} else {
				interval = (interval * 3) / nremoved
			}
			// limit
			if interval > 60 {
				interval = 60
			}
			if interval < 1 {
				interval = 1
			}
		}
	}
	go scheduler()

	return &chain
}
