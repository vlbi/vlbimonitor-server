// This file is part of the Radboud Radio Lab VLBI Monitor
// Copyright (C) 2016 Pim Schellart
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package datastore

import (
	"container/list"
	"database/sql"
	"github.com/xeipuuv/gojsonschema"
	"log"
)

type DataStore struct {
	pgSession     *sql.DB
	observatories []string
	schema        *gojsonschema.Schema
	caches        map[string]*Cache
	vexfiles      *list.List
	idReplay      string
}

func NewDataStore(pgSession *sql.DB, schemafile string, observatories []string) *DataStore {
	ds := DataStore{pgSession: pgSession, observatories: observatories}

	// init caches
	ds.caches = make(map[string]*Cache, len(observatories))
	for _, obs := range observatories {
		ds.caches[obs] = newCache(obs)
	}

	// init vexfiles slice
	ds.vexfiles = list.New()

	// load schema
	schemaLoader := gojsonschema.NewReferenceLoader("file://" + schemafile)
	{
		var err error
		if ds.schema, err = gojsonschema.NewSchema(schemaLoader); err != nil {
			log.Fatal("NewSchema:", err)
		}
	}

	// init postgres tables and caches
	if err := initRawdata(ds.pgSession); err != nil {
		log.Fatal("initRawdata:", err)
	}
	if err := initTimeseries(ds.pgSession, observatories); err != nil {
		log.Fatal("initTimeseries:", err)
	}
	if err := initClient(ds.pgSession); err != nil {
		log.Fatal("initClient:", err)
	}

	// restore clients cache from DB
	if headers, err := getClients(ds.pgSession); err != nil {
		log.Fatal("getClients:", err)
	} else {
		for _, h := range headers {
			ds.caches[h.Facility].headers.put(&h)
		}
	}

	// restore snapshot cache (state of all variables) from DB
	if snapshot, err := getState(ds.pgSession, observatories); err != nil {
		log.Println("InitDatastorePG: getCurrentSnapshot failed:", err)
	} else {
		for k, v := range snapshot {
			ds.caches[k].datachain.push(v)
		}
	}

	return &ds
}
