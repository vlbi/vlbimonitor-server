package datastore

import (
	"database/sql"
	"time"
)

const sql_CreateTableClient string = `CREATE TABLE IF NOT EXISTS client (
	id SERIAL PRIMARY KEY,
	facility TEXT NOT NULL,
	name TEXT NOT NULL,
	ipaddr TEXT,
	time INTEGER,
	version TEXT,
	versiontime INTEGER,
CONSTRAINT instance UNIQUE (facility, name, ipaddr)
)`

func initClient(db *sql.DB) error {
	_, err := db.Exec(sql_CreateTableClient)
	return err
}

func putClient(db *sql.DB, facility, clientname, clientip, version string, trecv int64) error {
	// w/o version
	if len(version) == 0 {
		sql := `INSERT INTO client (facility, name, ipaddr, time)
			VALUES ($1, $2, $3, $4)
		ON CONFLICT ON CONSTRAINT instance
		DO UPDATE
			SET time = $4`
		_, err := db.Exec(sql, facility, clientname, clientip, trecv)
		return err
	}

	// with version
	sql := `INSERT INTO client (facility, name, ipaddr, time, version, versiontime)
		VALUES ($1, $2, $3, $4, $5, $4)
	ON CONFLICT ON CONSTRAINT instance
	DO UPDATE
		SET time = $4, version = $5, versiontime = $4`
	_, err := db.Exec(sql, facility, clientname, clientip, trecv, version)
	return err
}

func getClients(db *sql.DB) ([]Header, error) {
	sql := `SELECT facility, name, ipaddr, time, version, versiontime FROM client`
	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}
	defer rows.Close() // good practice, though rows are consumed below

	result := make([]Header, 0)
	var facility, clientname, clientip, version string
	var t, versiontime int64
	for rows.Next() {
		rows.Scan(&facility, &clientname, &clientip, &t, &version, &versiontime)
		h := Header{
			Facility:      facility,
			Username:      clientname,
			ClientVersion: version,
			RecvTime:      time.Unix(t, 0),
		}
		result = append(result, h)
	}
	return result, nil
}
