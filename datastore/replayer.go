package datastore

import (
	"bitbucket.org/vlbi/vlbimonitor-server/replayapi"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// execute replay job
func (ds DataStore) Replayer(replay replayapi.Job) {
	defer close(replay.Done)

	t0 := time.Now()

	var after <-chan time.Time

	tuntil := replay.Start - time.Now().Unix()
	after = time.After(time.Duration(tuntil) * time.Second)
	select {
	case <-replay.Cancel:
		return
	case <-after:
	}

	// use all observatories if none are explicitly selected
	if len(replay.Observatories) == 0 {
		replay.Observatories = ds.observatories
	}

	// query
	tstart, tend := time.Unix(replay.OriginStart, 0), time.Unix(replay.OriginStop, 0)
	recs, n := GetRawsPG(ds.pgSession, replay.Cancel, replay.Observatories, &tstart, &tend, 0)

	if recs == nil {
		log.Printf("replay query empty: id=%s", replay.Id)
		return
	}

	// verbose
	duration := (replay.OriginStop - replay.OriginStart) / replay.Speed // calculate again, the values may have changed
	log.Printf("replay started: id=%s, nitems=%d, duration=%ds", replay.Id, n, duration)

	// reuse a TCP connection cached by http.Client
	client := &http.Client{
		Transport: &http.Transport{
			// heartbeat is 10 minutes by default
			IdleConnTimeout: 11 * time.Minute,
		},
		// prevent hanging if dest doesn't respond
		Timeout: time.Second * 10,
	}

	// loop over query items
	var tnext, torig int64
	var dnext time.Duration
	var rec *RecordIn
	var i int
	var ok, hush bool
	for {
		i += 1
		rec, ok = <-recs
		if !ok {
			break
		}

		torig = rec.Header.tDataMax
		tnext = (torig-replay.OriginStart)/replay.Speed + replay.Start
		dnext = time.Until(time.Unix(tnext, 0))
		after = time.After(dnext)
		if i == 1 || replay.Verbose {
			log.Printf("next item %d/%d: until=%.2f, jobid=%s, obs=%s", i, n, dnext.Seconds(), replay.Id, rec.Header.Facility)
		}

		select {
		case <-replay.Cancel:
			return
		case <-after:
			//log.Printf("submit item %d/%d: jobid=%s, obs=%s", i, n, replay.Id, rec.Header.Facility)
			// timeshift data timestamps
			if !replay.KeepOriginalDataTimestamps {
				dt := float64(replay.Start - replay.OriginStart)
				timeshiftData(rec.Data, dt)
			}
			if err := submit(client, replay.ServerURL, rec, replay.Id); err == nil {
				if hush {
					log.Printf("replay submission error resolved: id=%s", replay.Id)
					hush = false
				}
			} else {
				if !hush {
					log.Printf("replay submission error: id=%s; %v", replay.Id, err)
					hush = true
				}
			}
		}
	}
	log.Printf("replay finished: id=%s, timing=%.2fs", replay.Id, time.Since(t0).Seconds())
}

// timeshiftData updates datapoint timestamps with a given time delta
func timeshiftData(data map[string]DataArr, dt float64) {
	var then float64
	// loop over data parameters
	for k, _ := range data {
		// loop over array with data points
		for i, _ := range data[k] {
			then = data[k][i][0].(float64)
			data[k][i][0] = then + dt
		}
	}
}

func submit(client *http.Client, serverURL string, payload *RecordIn, id string) error {
	var buf bytes.Buffer
	json.NewEncoder(&buf).Encode(payload.Data)

	// raw endpoint
	url := fmt.Sprintf("%s/raw/%s", serverURL, payload.Header.Facility)

	// setup request
	var err error
	var req *http.Request
	req, err = http.NewRequest("POST", url, &buf)
	if err != nil {
		return err
	}

	// set replay header field
	req.Header.Set("replay-id", id)

	// execute request
	var resp *http.Response
	resp, err = client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// consume body to make client reuse the TCP connection
	body, _ := ioutil.ReadAll(resp.Body)

	// require successful submission
	if resp.StatusCode != 200 {
		return fmt.Errorf("%s: %s", resp.Status, string(body))
	}

	return nil
}
