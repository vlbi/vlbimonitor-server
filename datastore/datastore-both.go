package datastore

import (
	"bitbucket.org/vlbi/vlbimonitor-server/datastore/compress"
	"fmt"
	"log"
	"time"
)

func (ds DataStore) PutRecord(facility, clientname, clientip, version string, trecv int64, data map[string]DataArr, body []byte) (string, error) {
	// insert client info
	if err := putClient(ds.pgSession, facility, clientname, clientip, version, trecv); err != nil {
		log.Println("putClient:", err)
	}

	// insert into rawdata
	id, err := putRaw(ds.pgSession, facility, clientname, trecv, data, body)
	if err != nil {
		return "", fmt.Errorf("PutRaw: %v", err)
	}
	// return pg ID
	return fmt.Sprintf("%d", id), nil
}

func (ds DataStore) GetDataHistories(observatories []string, fields []string, startTime *time.Time, endTime *time.Time, nDatapoints int) (map[string]map[string]DataArr, error) {
	x0, x1 := startTime.Unix(), endTime.Unix()
	tsmapmap, err := GetTimeseriesPG(ds.pgSession, observatories, fields, x0, x1)
	if err != nil {
		return nil, err
	}

	// compress and transpose
	result := make(map[string]map[string]DataArr)
	for obs, m := range tsmapmap {
		result[obs] = make(map[string]DataArr)
		for k, ts := range m {
			// compress timeseries
			ts.x, ts.y, err = compress.Compress(ts.x, ts.y, nDatapoints, x0, x1)
			if err != nil {
				log.Println("GetDataHistories: Compress:", obs, k, len(ts.x), err)
			}

			// map x and y to array of tuples
			da := make(DataArr, 0, len(ts.x))
			for i := 0; i < len(ts.x); i++ {
				da = append(da, [2]interface{}{ts.x[i], ts.y[i]})
			}
			result[obs][k] = da
		}
	}
	return result, nil
}
