package datastore

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/lib/pq"
	"log"
	"sync"
	"time"
)

// max duration between updateTimeseries calls
const TS_UPDATE_SINCE_MAX = 600 * time.Second

// number of data points in a full timeseries row
// Changing these values requires rebuilding the timeseries table!
const TS_ROWLENGTH_MIN = 1000
const TS_ROWLENGTH_MAX = 2000

// number of workers per observatory
const TS_N_WORKERS_MAX = 10

// number of big-record input records, affect memory usage
const TS_MERGE_N_RECORDS_MAX = 10000000

// allow only one active update process at a time
// Each update will query the timeseries table for the last known ID and
// insert anything that is more recent than that.  Running simultaneous
// updates would would lead to double insertions.
var ts_update_mutex sync.RWMutex

// time of last update
var ts_update_t_last time.Time

// Data (x,y) points are saved as timeseries with separate x and y arrays.
//
// The x array saves timestamps in seconds since 1970-01-01 as integers.
// Sub-second resolution is not needed at this point but can easily be
// achieved by switching to floats later.
//
// The y array is stored as binary json in order to support different data
// types for different variables (on different rows).
const sql_CreateTableTimeseries string = `CREATE TABLE IF NOT EXISTS timeseries (
	id SERIAL PRIMARY KEY,
	facility TEXT NOT NULL,
	varname TEXT NOT NULL,
	timemin INTEGER NOT NULL,
	timemax INTEGER NOT NULL,
	idrawlast BIGINT NOT NULL,
	x INTEGER[],
	data JSONB
)`

// initTimeseries creates required tables and fire up a go routine that will
// perform periodic updates.
func initTimeseries(db *sql.DB, observatories []string) error {
	if _, err := db.Exec(sql_CreateTableTimeseries); err != nil {
		return err
	}

	// start worker to run updateTimeseries periodically
	ts_update_t_last = time.Now()
	go func() {
		for {
			if time.Now().After(ts_update_t_last.Add(TS_UPDATE_SINCE_MAX)) {
				updateTimeseries(db, observatories)
			}
			time.Sleep(time.Until(ts_update_t_last.Add(TS_UPDATE_SINCE_MAX)))
		}
	}()

	return nil
}

// updateTimeseries adds new records from rawdata to the timeseries table
func updateTimeseries(db *sql.DB, observatories []string) {
	// allow only one active update process at a time
	ts_update_mutex.Lock()
	defer ts_update_mutex.Unlock()

	// init with zeros
	idSince := make(map[string]uint64)
	for _, obs := range observatories {
		idSince[obs] = 0
	}

	// overwrite with any current values
	sql := `SELECT facility, MAX(idrawlast) FROM timeseries
	WHERE facility = ANY($1)
	GROUP BY facility`
	rows, err := db.Query(sql, pq.Array(observatories))
	if err != nil {
		log.Println("updateTimeseries:", err)
		return
	}
	defer rows.Close() // good practice, though rows are unconditionally consumed below
	var facility string
	var idrawmax uint64
	for rows.Next() {
		if err := rows.Scan(&facility, &idrawmax); err != nil {
			log.Println("updateTimeseries:", err)
			continue
		}
		idSince[facility] = idrawmax
	}

	// update timeseries for each obs concurrently
	var wg sync.WaitGroup
	for obs, idsince := range idSince {
		wg.Add(1)
		go func(obs string, idsince uint64) {
			updateOneobsTimeseries(db, obs, idsince)
			wg.Done()
		}(obs, idsince)
	}

	// wait for completion
	t0 := time.Now()
	wg.Wait()
	ts_update_t_last = time.Now()

	// print if took long
	if time.Since(t0).Seconds() > 1 {
		log.Printf("updateTimeseries: Done: n_obs=%d t_tot=%.3fs", len(observatories), time.Since(t0).Seconds())
	}
}

// updateOneobsTimeseries adds new records to one observatory's timeseries
// 1. find new records
// 2. merge new records to one big record holding timeseries for all variables
// 3. for each variable, insert the new timeseries data
func updateOneobsTimeseries(db *sql.DB, obs string, idSince uint64) {
	// new raw ids
	cancel := make(chan struct{})
	defer close(cancel)
	recs, nrec := GetRawsPG(db, cancel, []string{obs}, nil, nil, idSince)
	// print if interesting
	if nrec > 1000 {
		log.Printf("updateOneobsTimeseries: New records: obs=%s id_since=%d n_rec=%d\n", obs, idSince, nrec)
	}

	// merge records and insert
	// chunk this if memory is limited
	var ttot time.Duration
	keys := make(map[string]struct{}) // keep track of all keys
	errs := make([]string, 0)
	for {
		// create one big data record out of all small ones
		das, idLast, doMore := mergeRecords(cancel, recs, TS_MERGE_N_RECORDS_MAX)

		// concurrently insert data for each var independently
		type Work struct {
			k string
			v Timeseries
		}
		work := make(chan *Work, len(das))
		result := make(chan error, len(das))
		// insert work
		for k, v := range das {
			keys[k] = struct{}{} // save key names
			o := Work{k: k, v: v.ToTimeseries()}
			work <- &o
		}
		close(work)
		// start nworkers workers
		nworkers := len(das)
		if nworkers > TS_N_WORKERS_MAX {
			nworkers = TS_N_WORKERS_MAX
		}
		for i := 1; i <= nworkers; i++ {
			go func() {
				for o := range work {
					err := updateOnevarTimeseriesWrap(db, obs, o.k, o.v.x, o.v.y, idLast)
					result <- err
				}
			}()
		}
		// wait for all workers to finish
		t_ := time.Now()
		for range das {
			if err := <-result; err != nil {
				errs = append(errs, err.Error())
			}
		}
		close(result)
		ttot += time.Since(t_)

		// done inserting
		if !doMore {
			break
		}
	}

	// print if took long
	if ttot.Seconds() > 1 {
		log.Printf("updateOneobsTimeseries: All inserted: obs=%s n_rec=%d n_var=%d t_tot=%.3fs", obs, nrec, len(keys), ttot.Seconds())
	}
}

// updateOnevarTimeseries adds new data to one varname+observatory's timeseries
// 1. find rows that overlap in time with new data
// 2. for each row, given that it is an open (head) or closed (tail or middle) row time window
//   1. download the row's data points
//   2. interleave by time the downloaded data points with new data points that fall in the row's time window
//   3. append any remaining data to the open head row
//   4. insert extended row into timeseries table
//
// Note: An open or closed time window means that the window can or can not be
// extended into the future.  Extending into the past is always possible since
// all rows that overlap are already included in the selected rows.
func updateOnevarTimeseries(db *sql.DB, stmt *sql.Stmt, obs, varname string, x []int64, y []interface{}, idrawmax uint64) error {
	// find rows with data that is contemporary with our current data
	// also include the last row to cover the case where there is no overlap
	sql := `WITH ts AS (
		SELECT * FROM timeseries
		WHERE facility = $1 AND varname = $2)
	(SELECT id, x, data FROM ts
		WHERE timemax > $3 AND timemin < $4
		ORDER BY timemax)
	UNION
	(SELECT id, x, data FROM ts
		ORDER BY timemax DESC
		FETCH FIRST ROW ONLY)`
	rows, err := db.Query(sql, obs, varname, x[0], x[len(x)-1])
	if err != nil {
		log.Println("updateOnevarTimeseries: Query:", obs, varname, err)
		return err
	}
	defer rows.Close() // not all rows may be consumed

	// timing
	t0 := time.Now()

	// interleave old with new data and insert replacing the original rows
	var idq uint64
	var yencq []byte
	var isHeadRow bool // Only the last row belonging to a timeseries can be shorther than TS_ROWLENGTH_MIN

	var tsq, tsin Timeseries
	var i0, i1 int
	m := TS_ROWLENGTH_MAX + len(x)
	for {
		tsq = Timeseries{
			x: make([]int64, 0, m-i1),
			y: make([]interface{}, 0, m-i1),
		}
		// read row if possible
		// else fake empty row
		if ok := rows.Next(); ok {
			if err = rows.Scan(&idq, pq.Array(&tsq.x), &yencq); err != nil {
				log.Println("updateOnevarTimeseries: Scan:", idq, len(tsq.x), err)
			}
			if err := json.Unmarshal(yencq, &tsq.y); err != nil {
				log.Println("updateOnevarTimeseries: Unmarshal:", len(tsq.x), len(tsq.y), err)
			}
		} else if i1 == 0 {
			idq = 0
		} else {
			break
		}

		// Only the last row belonging to a timeseries can be shorter than TS_ROWLENGTH_MIN
		isHeadRow = len(tsq.x) < TS_ROWLENGTH_MIN

		// slice new data to fit the row's time window
		// open row
		if isHeadRow {
			i1 = len(x) // upper bound
		} else {
			// closed row
			x1 := tsq.x[len(tsq.x)-1]
			for ; i1 < len(x); i1++ {
				if x[i1] > x1 {
					break
				}
			}
		}
		tsin = Timeseries{x: x[i0:i1], y: y[i0:i1]}
		i0 = i1 // next lower bound

		// interleave incoming and old (query) data points
		tsq = tsq.MergeFromRight(tsin)

		// Insert this row now that all points are interleaved
		updateOnerowTimeseries(db, stmt, idq, obs, varname, tsq.x, tsq.y, idrawmax, isHeadRow)
	}

	// timing
	if time.Since(t0).Seconds() > 1 {
		log.Printf("Inserted: obs=%s varname=%s len=%d time=%.3fs", obs, varname, len(x), time.Since(t0).Seconds())
	}

	// verify that all new points got inserted
	if i1 != len(x) {
		return fmt.Errorf("not all points inserted: varname=%s nnew=%d/%d", varname, i1, len(x))
	}
	return nil
}

// updateOnevarTimeseriesWrap sets up a prepared statement to be used by the
// updateOnevarTimeseries function.
//
// It turns out that the concurrent use of transactions is slower than
// concurrent use of the DB directly.  We parallelize over variables within a
// facility.  The facility-wide "oneobs" transactions turn out to slow down
// insertions by a factor 4-5 compared to "onevar" transactions on an 8-core
// machine.
//
// The "oneobs" transaction has the additional downside that the same
// transaction may fail again on the next attempt, thus blocking updates to
// the whole facility until the error is noticed and resolved.
//
// The lower level "onevar" transaction, upon failure, will not be retried
// forever because the idrawlast will be set on rows for other variables.  The
// data that failed to be inserted will be missing from the timeseries table
// until rows with idrawlast>id_bad for that particular facility are removed.
//
// We log idrawlast for the transaction that failed so that this in principle
// can be repaired without removing all rows for that facility.
func updateOnevarTimeseriesWrap(db *sql.DB, obs, varname string, x []int64, y []interface{}, idrawmax uint64) error {
	tx, err := db.Begin()
	if err != nil {
		log.Println("updateOneobsTimeseries: Begin:", obs, err)
		return err
	}
	stmt, err := tx.Prepare(pq.CopyIn("timeseries", "facility", "varname", "timemin", "timemax", "idrawlast", "x", "data"))
	if err != nil {
		log.Println("updateOneobsTimeseries: Prepare:", obs, err)
		goto closeTx
	}

	if err := updateOnevarTimeseries(db, stmt, obs, varname, x, y, idrawmax); err != nil {
		log.Println("updateOneobsTimeseries: Insert", obs, err)
	} else {
		// flush buffer and close statement
		if _, err := stmt.Exec(); err != nil {
			log.Println("updateOneobsTimeseries: Flush:", obs, err)
		}
	}

	if err := stmt.Close(); err != nil {
		log.Println("updateOneobsTimeseries: StmtClose:", obs, err)
	}

	// commit or rollback transaction
closeTx:
	if err != nil {
		log.Printf("updateOneobsTimeseries: rolling back: obs=%s: %s\n", obs, err)
		if err := tx.Rollback(); err != nil {
			log.Println("updateOneobsTimeseries: Rollback:", obs, err)
			return err
		}
		return err
	}
	if err := tx.Commit(); err != nil {
		log.Println("updateOneobsTimeseries: Commit:", obs, err)
		return err
	}
	return nil
}

// updateOnerowTimeseries inserts new rows, replacing an existing row if
// possible.
//
// For each timeseries, one head row exists.  Once the row grows beyond MIN it
// gets truncated at that length, the time window closes, and a new row
// becomes the head row.  The head row is identified by its length < MIN.
//
// Non-head rows can grow within their closed time window.  If the length
// grows beyond MAX, additional rows of length MIN are inserted.  Rows that
// are created this way may never be shorter than MIN, because that would let
// a row appear as an open head row.
func updateOnerowTimeseries(db *sql.DB, stmt *sql.Stmt, id uint64, obs, varname string, x []int64, y []interface{}, idrawmax uint64, isHeadRow bool) {
	var i int  // not inserted yet
	var ii int // end of range to be inserted
	for {
		// done
		if i == len(x) {
			break
		}

		// set endpoint for current insert
		if len(x)-i < TS_ROWLENGTH_MIN {
			ii = len(x) // short endrow
		} else if !isHeadRow && len(x)-i < TS_ROWLENGTH_MAX {
			ii = len(x) // long middle row
		} else {
			ii += TS_ROWLENGTH_MIN // standard length row
		}

		// encode y
		yenc, err := json.Marshal(y[i:ii])
		if err != nil {
			log.Println("updateOnerowTimeseries: Marshal:", obs, varname, i, ii, err)
			continue
		}

		// replace original row once
		if id != 0 {
			sql := `UPDATE timeseries
				SET timemin=$2, timemax=$3, idrawlast=$4, x=$5, data=$6
			WHERE id=$1`
			if _, err := db.Exec(sql, id, x[i], x[ii-1], idrawmax, pq.Array(x[i:ii]), yenc); err != nil {
				log.Println("updateOnerowTimeseries: Exec1:", obs, varname, i, ii, err)
			}
			id = 0 // replace original row only once
			i = ii
			continue
		}

		// insert additional rows
		if _, err := stmt.Exec(obs, varname, x[i], x[ii-1], idrawmax, pq.Array(x[i:ii]), yenc); err != nil {
			log.Println("updateOnerowTimeseries: Exec:", obs, varname, i, ii, err)
		}
		i = ii
	}
}

// GetTimeseriesPG retrieves the timeseries for a list of observatories and
// fields.
func GetTimeseriesPG(db *sql.DB, observatories []string, fields []string, x0, x1 int64) (map[string]map[string]Timeseries, error) {
	// query DB
	sql := `SELECT facility, varname, x, data FROM timeseries
	WHERE facility = ANY($1) AND varname = ANY($2) AND timemax >= $3 AND timemin <= $4
	ORDER BY facility, varname, timemax`
	rows, err := db.Query(sql, pq.Array(observatories), pq.Array(fields), x0, x1)
	if err != nil {
		return nil, err
	}
	defer rows.Close() // good practice, though rows are unconditionally consumed below

	// Reconstruct complete timeseries from multiple rows
	result := make(map[string]map[string]Timeseries)
	var obs, field string
	var x []int64
	var yenc []byte
	var y []interface{}
	var i0, i1 int
	var ts Timeseries
	for rows.Next() {
		rows.Scan(&obs, &field, pq.Array(&x), &yenc)
		if err := json.Unmarshal(yenc, &y); err != nil {
			log.Println("GetDataHistories: Unmarshal:", err)
		}

		// map not allocated yet
		if _, ok := result[obs]; !ok {
			result[obs] = make(map[string]Timeseries)
		}
		ts = result[obs][field]

		// only use points inside the requested time range
		for i0 = 0; i0 < len(x); i0++ {
			if x[i0] >= x0 {
				// the state at x==x0 may be determined by the last point before x0
				if x[i0] > x0 && i0 > 0 {
					i0 -= 1
				}
				break
			}
		}
		for i1 = len(x); i1 > 0; i1-- {
			if x[i1-1] <= x1 {
				break
			}
		}
		ts.x = append(ts.x, x[i0:i1]...)
		ts.y = append(ts.y, y[i0:i1]...)
		result[obs][field] = ts
	}

	return result, nil
}

// GetState determines the current values of all variables for
// a list of observatories.
//
// This function is only called on startup.  User requests are served from the
// cache incrementally.
func getState(db *sql.DB, observatories []string) (map[string]Dataflat, error) {
	t0 := time.Now()

	// select timemax (instead of x[-1]) and last point in data for each obs+var combo
	sql := `SELECT a.facility, a.varname,  a.timemax, a.data->-1 FROM timeseries a
	INNER JOIN (
		SELECT facility, varname, MAX(timemax) AS t FROM timeseries
		WHERE facility = ANY($1)
		GROUP BY facility, varname
	) b
		ON a.facility = b.facility AND a.varname = b.varname AND a.timemax = b.t`

	// slower alternative
	//sql := `SELECT a.facility, a.varname,  a.timemax, a.data->>-1 FROM timeseries a
	//LEFT JOIN timeseries b
	//	ON a.facility = b.facility AND a.varname = b.varname AND a.timemax < b.timemax
	//WHERE a.facility = ANY($1) AND b.id IS NULL
	//ORDER BY a.facility`

	rows, err := db.Query(sql, pq.Array(observatories))
	if err != nil {
		return nil, err
	}
	defer rows.Close() // not all rows may be consumed

	// prepare snapshot
	snapshot := make(map[string]Dataflat)
	for _, obs := range observatories {
		snapshot[obs] = make(Dataflat)
	}

	var facility, varname string
	var x float64 // TODO: we still mix ints and floats
	var yenc []byte
	var y interface{}
	for rows.Next() {
		if err := rows.Scan(&facility, &varname, &x, &yenc); err != nil {
			return nil, err
		}
		if err := json.Unmarshal(yenc, &y); err != nil {
			return nil, err
		}
		snapshot[facility][varname] = [2]interface{}{x, y}
	}

	// print if took long
	if time.Since(t0).Seconds() > 1 {
		log.Printf("GetState: n_obs=%d t_tot=%.3fs", len(observatories), time.Since(t0).Seconds())
	}

	return snapshot, nil
}
