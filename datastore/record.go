// This file is part of the Radboud Radio Lab VLBI Monitor
// Copyright (C) 2016 Pim Schellart
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package datastore

import (
	"time"
)

type StatusRecord struct {
	Clients  map[string]ClientStat   `json:"clients,omitempty" bson:"lastUpdate"`
	Data     Dataflat                `json:"data,omitempty"`
	Messages map[string]MessagePoint `json:"messages,omitempty"`
}

type ClientStat struct {
	RecvTime int64  `json:"recvTime" bson:"updateTime"` //last seen time
	Version  string `json:"version,omitempty" bson:"version"`
}

type MessagePoint struct {
	RecvTime time.Time `json:"recvTime,omitempty"`
	Username string    `json:"username,omitempty"`
	Content  string    `json:"content,omitempty"`
}
