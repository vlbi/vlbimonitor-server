package compress

import (
	"fmt"
	"log"
	"time"
)

// Compress reduces the number of data points provided to a target number of
// npoints.  It first tries to drop duplicates (i.e. repeating y values) and
// if necessary rebins the result onto a regular grid on a time window from x0
// to x1.
// The x-coordinate has units of seconds since 1970-01-01 UTC.
func Compress(x []int64, y []interface{}, npoints int, x0, x1 int64) ([]int64, []interface{}, error) {
	// Compression not necessary
	if len(x) <= npoints || npoints == 0 {
		return x, y, nil
	}

	// sanity check
	if len(x) != len(y) {
		return x, y, fmt.Errorf("Compress: len(x) != len(y)")
	}

	// deduplicate timeseries
	//n_orig := len(x)
	x, y = deduplicate(x, y)
	//n_dedup := len(x)

	// good enough
	if len(x) <= npoints {
		//log.Printf("Compress: n=%d n_dedup=%d n_target=%d\n", n_orig, n_dedup, npoints)
		return x, y, nil
	}

	// rebin timeseries
	var ok bool
	var err error
	switch y[0].(type) {
	case float64:
		ycast := make([]float64, len(y))
		// cast to float64 type
		for i, y_ := range y {
			ycast[i], ok = y_.(float64)
			if !ok {
				log.Println("Compress float64: x=%d y=%v.(%T)\n", x[i], y[i], y[i])
				return x, y, nil
			}
		}
		var yy []float64
		x, yy, _, _, err = rebinFloat(x, ycast, npoints, x0, x1)
		// cast back to interface{} type
		y = y[:0]
		for _, y_ := range yy {
			y = append(y, y_)
		}
	case int:
		ycast := make([]int, len(y))
		// cast to float64 type
		for i, y_ := range y {
			ycast[i], ok = y_.(int)
			if !ok {
				log.Println("Compress int: x=%d y=%v.(%T)\n", x[i], y[i], y[i])
				return x, y, nil
			}
		}
		var yy []int
		x, yy, _, _, err = rebinInt(x, ycast, npoints, x0, x1)
		// cast back to interface{} type
		y = y[:0]
		for _, y_ := range yy {
			y = append(y, y_)
		}
	}
	//n_rebin := len(x)
	//log.Printf("Compress: n=%d n_dedup=%d n_rebin=%d n_target=%d\n", n_orig, n_dedup, n_rebin, npoints)

	return x, y, err
}

// deduplicate drops data points with repeating y values, only the first and
// last point with a certain value are kept.
func deduplicate(x []int64, y []interface{}) ([]int64, []interface{}) {
	// points to be saved
	index := make([]int, 0, len(x))

	// first point
	index = append(index, 0)
	for i := 1; i < len(x)-1; i++ {
		// sanity check
		if x[i] < x[i-1] {
			fmt.Printf("Deduplicate: before='%v' after='%v'\n", time.Unix(int64(x[i-1]), 0), time.Unix(int64(x[i]), 0))
		}
		// duplicate point
		if y[i] == y[i-1] && y[i] == y[i+1] {
			continue
		}
		// save
		index = append(index, i)
	}
	// last point
	index = append(index, len(x)-1)

	// compress
	for i, j := range index {
		x[i] = x[j]
		y[i] = y[j]
	}
	x = x[:len(index)]
	y = y[:len(index)]

	return x, y
}

// Due to the lack of generics in go, the rebin function needs to be split
// into a float and an int version.  In order to facilitate easy diffs between
// these two functions (which are identical apart from their types) these are
// stored in separate files.
