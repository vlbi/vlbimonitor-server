package compress

import (
	"fmt"
)

// rebinInt rebins data points with y-datatype int on a regular grid.
func rebinInt(xin []int64, yin []int, npoints int, x0, x1 int64) ([]int64, []int, []int, []int, error) {
	// target array
	x := make([]int64, 0, npoints)
	y := make([]int, 0, npoints)  // mean
	yn := make([]int, 0, npoints) // min
	yx := make([]int, 0, npoints) // max

	var xx0 int64
	var ysum, ymin, ymax int
	var n int
	xx1 := x0
	ntot := 0
	for i, j := 0, 0; j < npoints && i < len(yin); j++ {
		// reset counters
		ysum, ymin, ymax = 0, yin[i], yin[i]
		n = 0
		// bin bounds
		xx0 = xx1
		xx1 = x0 + ((x1-x0)*int64(j))/int64(npoints-1)

		// sum da points
		for ; i < len(xin) && xin[i] <= xx1; i++ {
			ysum += yin[i]
			if yin[i] < ymin {
				ymin = yin[i]
			}
			if yin[i] > ymax {
				ymax = yin[i]
			}
			n += 1
			// sanity check
			if xin[i] < xx0 {
				return xin, yin, yin, yin, fmt.Errorf("compression error: timeseries not monotonic")
			}
		}

		ntot += n

		// save
		if n > 0 {
			x = append(x, (xx0+xx1)/2)
			y = append(y, ysum/n)
			yn = append(yn, ymin)
			yx = append(yx, ymax)
		}
	}

	// sanity check
	if ntot != len(xin) {
		return xin, yin, yin, yin, fmt.Errorf("compression error: %d out of %d points mapped", ntot, len(xin))
	}

	return x, y, yn, yx, nil
}
