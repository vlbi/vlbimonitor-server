package datastore

// Timeseries contains the x and y coordinates of the data points in a
// timeseries in two separate arrays.
type Timeseries struct {
	x []int64
	y []interface{}
}

// Merge b into a, growing a to the required capacity if necessary
// This function is the Timeseries analog of DataArr.MergeFromRight.
func (a Timeseries) MergeFromRight(b Timeseries) (ts Timeseries) {
	na := len(a.x)
	nb := len(b.x)

	// find insertion index
	index := make([]int, nb)
	for i, j := na, nb-1; i > 0 && j >= 0; {
		if a.x[i-1] < b.x[j] {
			index[j] = i // beginning of section
			j -= 1
			continue
		}
		i -= 1
	}

	// insert points at indexed locations
	// grow backing array
	if na+nb > cap(a.x) {
		slc := make([]int64, na, na+nb)
		copy(slc, a.x)
		a.x = slc
	}
	if na+nb > cap(a.y) {
		slc := make([]interface{}, na, na+nb)
		copy(slc, a.y)
		a.y = slc
	}
	// grow slice
	a.x = a.x[:na+nb]
	a.y = a.y[:na+nb]
	// relocate and insert
	ii := na // beginning of next section
	for j := nb - 1; j >= 0; j-- {
		i := index[j] // beginning of section
		copy(a.x[i+j+1:], a.x[i:ii])
		a.x[i+j] = b.x[j]
		copy(a.y[i+j+1:], a.y[i:ii])
		a.y[i+j] = b.y[j]
		ii = i
	}
	return Timeseries{x: a.x, y: a.y}
}
