package datastore

import (
	"bitbucket.org/vlbi/vlbimonitor-server/userapi"
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/xeipuuv/gojsonschema"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (ds *DataStore) SnapshotGetHandle(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// recvTime cookie
	var tfrom int64
	if cookie, _ := r.Cookie("snap_recvTime"); cookie != nil {
		var err error
		tfrom, err = strconv.ParseInt(cookie.Value, 10, 64)
		if err != nil {
			http.Error(w, "cookie value error: recvtime: "+err.Error(), http.StatusBadRequest)
			return
		}
	}
	// set response recvtime cookie
	cookie := http.Cookie{Name: "snap_recvTime", Value: fmt.Sprintf("%d", time.Now().Unix()), Path: "/"}
	http.SetCookie(w, &cookie)

	// filter parameters
	if err := r.ParseForm(); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	observatories, ok := r.Form["observatory"]
	// use all observatories by default
	if !ok {
		observatories = ds.observatories
	}

	snapshots := make(map[string]interface{}, len(observatories))

	for _, obs := range observatories {
		cache, ok := ds.caches[obs]
		if !ok {
			continue // skip unknown facility
		}

		ss := StatusRecord{
			Data:     cache.datachain.get(tfrom),
			Clients:  cache.headers.toMap(tfrom),
			Messages: cache.messages.toMap(tfrom),
		}
		// include non-empty snapshots only
		if len(ss.Data) != 0 || len(ss.Clients) != 0 || len(ss.Messages) != 0 {
			snapshots[obs] = ss
		}
	}

	// vexfiles
	vv := make([]VexfileInfo, 0)
	for e := ds.vexfiles.Front(); e != nil; e = e.Next() {
		v := e.Value.(VexfileInfo)
		if v.ReadTime < tfrom {
			break
		}
		vv = append(vv, v)
	}
	if len(vv) > 0 {
		snapshots["vexfiles"] = vv
	}

	// replay id
	w.Header().Set("replay-id", ds.idReplay)

	json.NewEncoder(w).Encode(snapshots)
}

func (ds *DataStore) TimeseriesHandle(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// HTTP header fields
	var viewportwidth int
	if s := r.Header.Get("Viewport-Width"); s != "" {
		var err error
		viewportwidth, err = strconv.Atoi(s)
		if err != nil {
			http.Error(w, fmt.Sprintf("value error: %s: %s", err.Error(), "Header.Viewport-Width"), http.StatusBadRequest)
			return
		}
	}

	// URL filter parameters
	if err := r.ParseForm(); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	// time winow
	var twindow [2]time.Time
	for i, par := range []string{"startTime", "endTime"} {
		val, err := strconv.ParseInt(r.FormValue(par), 10, 64)
		if err != nil {
			msg := fmt.Sprintf("value error: %s: %s", par, err.Error())
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		twindow[i] = time.Unix(val, 0)
	}
	// limited number of datapoints
	var nDatapoints int
	if s := r.FormValue("nDatapoints"); s != "" {
		nDatapoints, _ = strconv.Atoi(s) // produces 0 on error
	}
	if nDatapoints == 0 {
		nDatapoints = viewportwidth * 2 // 0 if not set
	}
	// arrays
	observatories, ok := r.Form["observatory"]
	if !ok {
		http.Error(w, "no observatory selected", http.StatusBadRequest)
		return
	}
	fields, ok := r.Form["field"]
	if !ok {
		http.Error(w, "no field selected", http.StatusBadRequest)
		return
	}

	// get result
	result, err := ds.GetDataHistories(observatories, fields, &twindow[0], &twindow[1], nDatapoints)
	if err != nil {
		log.Printf(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(result)
}

func (ds *DataStore) TimeseriesDownloadHandle(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()
	// time winow
	var twindow [2]time.Time
	for i, par := range []string{"startTime", "endTime"} {
		val, err := strconv.ParseInt(r.FormValue(par), 10, 64)
		if err != nil {
			msg := fmt.Sprintf("value error: %s: %s", par, err.Error())
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		twindow[i] = time.Unix(val, 0)
	}
	observatories, ok := r.Form["observatory"]
	if !ok {
		http.Error(w, "no observatory selected", http.StatusBadRequest)
		return
	}
	fields, ok := r.Form["field"]
	if !ok {
		http.Error(w, "no field selected", http.StatusBadRequest)
		return
	}

	// get result
	result, err := ds.GetDataHistories(observatories, fields, &twindow[0], &twindow[1], 0)
	if err != nil {
		log.Printf(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// set http header fields
	contdis := fmt.Sprintf("attachment; filename=vlbimon.%s.%s.txt", strings.Join(observatories, "-"), strings.Join(fields, "-"))
	w.Header().Set("Content-Disposition", contdis)
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))

	// write data header
	n := 0
	for _, obs := range result {
		n += len(obs)
	}
	l := 0
	for _, field := range fields {
		if len(field) > l {
			l = len(field)
		}
	}
	format := fmt.Sprintf("## %%8s   %%-%ds %%8d\n", l)
	fmt.Fprintln(w, "## n_timeseries:", n)
	fmt.Fprintln(w, "##")
	fmt.Fprintln(w, "## facility | varname | length")
	fmt.Fprintln(w, "## ---------+---------+-------")
	for _, obs := range observatories {
		for _, field := range fields {
			v, ok := result[obs][field]
			if !ok {
				continue
			}
			fmt.Fprintf(w, format, obs, field, len(v))
		}
	}
	fmt.Fprintln(w, "##")
	fmt.Fprintln(w, "## x: timestamp [seconds since 1970-01-01 UTC]")
	fmt.Fprintln(w, "## y: value")
	fmt.Fprintf(w, "## x_from:  %d (%s)\n", twindow[0].Unix(), twindow[0].UTC())
	fmt.Fprintf(w, "## x_until: %d (%s)\n", twindow[1].Unix(), twindow[1].UTC())
	fmt.Fprintln(w, "##")

	// write file body
	for _, obs := range observatories {
		for _, field := range fields {
			v, ok := result[obs][field]
			if !ok {
				continue
			}
			fmt.Fprintln(w, "#", obs, field, len(v))
			for _, point := range v {
				fmt.Fprintf(w, "%v %v\n", point[0], point[1])
			}
		}
	}
}

func (ds *DataStore) RawPostHandle(uc *userapi.UserCache) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		facility := ps.ByName("facility")

		version := r.Header.Get("ClientVersion")
		ds.idReplay = r.Header.Get("replay-id")

		// authorize the request for this facility
		clientname, clientip, err := uc.AuthorizeProvider(r, facility)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		// unmarshall json body
		body, _ := ioutil.ReadAll(r.Body)
		data := make(map[string]DataArr)
		if err := json.Unmarshal(body, &data); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// validate against schema
		docLoader := gojsonschema.NewGoLoader(&data)
		if result, err := ds.schema.Validate(docLoader); err != nil {
			log.Println("RpcPostHandle:", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else if !result.Valid() {
			for _, e := range result.Errors() {
				log.Printf("Validate: %s %s \"%s\"\n", e.Type(), e.Field(), e.Value())
			}
			msg := fmt.Sprintf("input does not match schema, facility=%s: %s", facility, result.Errors())
			log.Println("RpcPostHandle:", msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}

		// receive time
		trecv := time.Now().Unix()

		// verify facility
		cache, ok := ds.caches[facility]
		if !ok {
			http.Error(w, "unknown facility: "+facility, http.StatusUnauthorized)
			return
		}

		// update header cache
		h := Header{
			Facility:      facility,
			Username:      clientname,
			ClientVersion: version,
			RecvTime:      time.Unix(trecv, 0),
		}
		cache.headers.put(&h)

		// update data cache
		dataflat := make(Dataflat)
		for k, points := range data {
			dataflat[k] = points[len(points)-1]
		}
		if len(dataflat) > 0 {
			cache.datachain.push(dataflat)
		}

		// insert
		id, err := ds.PutRecord(facility, clientname, clientip, version, trecv, data, body)
		if err != nil {
			log.Printf("PutRecord ERROR: fac=%s user=%s ip=%s recid=%d, %s\n", facility, clientname, clientip, id, err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// reply with record id
		json.NewEncoder(w).Encode(id)
	}
}

func (ds *DataStore) RawGetHandle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
}

// SnapshotPatchHandle triggers updating the timeseries table
func (ds *DataStore) TimeseriesPatchHandle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.Println("Start handle update timeseries")
	//// serial version
	//if err := ds.UpdateTimeSeriesTables(); err != nil {
	//	log.Println("Error while updating timeseries tables:", err)
	//}
	updateTimeseries(ds.pgSession, ds.observatories)
	log.Println("Finish handle update timeseries")
	json.NewEncoder(w).Encode(fmt.Sprintf("Timeseries update finished"))
}
