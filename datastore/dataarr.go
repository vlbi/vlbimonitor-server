package datastore

import (
	"fmt"
	"time"
)

type DataArr [][2]interface{}

// implement the sort.Interface
func (da DataArr) Len() int           { return len(da) }
func (da DataArr) Swap(i, j int)      { da[i], da[j] = da[j], da[i] }
func (da DataArr) Less(i, j int) bool { return da[i][0].(float64) < da[j][0].(float64) }

// wrapper around rebin and deduplicate methods
func (da DataArr) Compress(npoints int) (DataArr, error) {
	// Nothing to compress if da is empty, or if there are less points than npoints
	if len(da) <= npoints || npoints == 0 {
		return da, nil
	}

	// wrong y type
	if _, ok := da[0][1].(float64); !ok {
		return da, nil
	}

	// wrong x type
	if _, ok := da[0][0].(float64); !ok {
		return nil, fmt.Errorf("compression error: DataArr[0].(type) not float64")
	}

	// deduplicate timeseries
	if dataNew, err := da.Deduplicate(npoints); err != nil {
		fmt.Println(err)
	} else if len(dataNew) < npoints {
		return dataNew, nil
	}

	// rebin timeseries
	return da.Rebin(npoints)
}

// drop some of the repeating values
func (da DataArr) Deduplicate(npoints int) (DataArr, error) {
	// target dx
	x0 := da[0][0].(float64)
	x1 := da[len(da)-1][0].(float64)
	dxx := (x1 - x0) / float64(npoints)

	// target array
	dataNew := make(DataArr, 0, len(da))

	dataNew = append(dataNew, da[0])
	x00 := da[0][0].(float64)
	for i := 1; i < len(da)-1; i++ {
		// sanity check
		if da[i][0].(float64) < da[i-1][0].(float64) {
			fmt.Println("before", time.Unix(int64(da[i-1][0].(float64)), 0))
			fmt.Println("after", time.Unix(int64(da[i][0].(float64)), 0))
			return nil, fmt.Errorf("compression error: timeseries not monotonic")
		}
		// duplicate point
		if da[i][1] == da[i-1][1] &&
			da[i][1] == da[i+1][1] &&
			da[i][0].(float64) < x00+dxx {
			continue
		}
		// save
		x00 = da[i][0].(float64)
		dataNew = append(dataNew, da[i])
	}
	dataNew = append(dataNew, da[len(da)-1])

	return dataNew, nil
}

// rebin data on a regular grid
func (da DataArr) Rebin(npoints int) (DataArr, error) {
	// copy to [][2]float64
	fdata := make([][2]float64, len(da))
	for i, d := range da {
		fdata[i] = [2]float64{d[0].(float64), d[1].(float64)}
	}

	// target array
	dataNew := make(DataArr, 0, npoints)

	x0 := da[0][0].(float64)
	x1 := da[len(da)-1][0].(float64)
	dxx := (x1 - x0) / float64(npoints)

	var xx0, ysum float64
	var n int
	xx1 := x0
	m := 0
	for i, j := 0, 0; j < npoints; j++ {
		// reset counters
		ysum = 0.
		n = 0
		// bin bounds
		xx0 = xx1
		xx1 = x0 + float64(j+1)*dxx

		// sum da points
		for ; i < len(da) && da[i][0].(float64) <= xx1; i++ {
			ysum += da[i][1].(float64)
			n += 1
			// sanity check
			if da[i][0].(float64) < xx0 {
				return nil, fmt.Errorf("compression error: timeseries not monotonic")
			}
		}

		m += n

		// save
		if n > 0 {
			xx := .5 * (xx0 + xx1)
			tuple := [2]interface{}{xx, ysum / float64(n)}
			dataNew = append(dataNew, tuple)
		}
	}

	// sanity check
	if m != len(fdata) {
		return nil, fmt.Errorf("compression error: %d out of %d points mapped", m, len(fdata))
	}

	return dataNew, nil
}

// convert DataArr to Timeseries
func (da DataArr) ToTimeseries() (ts Timeseries) {
	for _, point := range da {
		ts.x = append(ts.x, int64(point[0].(float64)))
		ts.y = append(ts.y, point[1])
	}
	return ts
}

// Merge b into a, growing a to the required capacity if necessary
// This function is the DataArr analog to Timeseries.MergeFromRight.
func (a DataArr) MergeFromRight(b DataArr) DataArr {
	// find insertion index
	index := make([]int, len(b))
	for i, j := len(a), len(b)-1; i > 0 && j >= 0; {
		if a[i-1][0].(float64) < b[j][0].(float64) {
			index[j] = i // beginning of section
			j -= 1
			continue
		}
		i -= 1
	}

	// insert points at indexed locations
	// grow backing array
	if len(a)+len(b) > cap(a) {
		slc := make(DataArr, len(a), len(a)+len(b))
		copy(slc, a)
		a = slc
	}
	// grow slice
	a = a[:len(a)+len(b)]
	// relocate and insert
	ii := len(a) // beginning of next section
	for j := len(b) - 1; j >= 0; j-- {
		i := index[j] // beginning of section
		copy(a[i+j+1:], a[i:ii])
		a[i+j] = b[j]
		ii = i
	}
	return a
}
