package datastore

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/lib/pq"
	"log"
	"math"
	"strings"
	"time"
)

const sql_CreateTableRaw string = `CREATE TABLE IF NOT EXISTS rawdata (
	id BIGSERIAL PRIMARY KEY,
	facility TEXT NOT NULL,
	clientname TEXT NOT NULL,
	trecv INTEGER NOT NULL,
	tdatamin INTEGER,
	tdatamax INTEGER,
	data JSONB
)`

const sql_CreateIndexRawFacility = `CREATE INDEX IF NOT EXISTS rawdata_facility_index ON public.rawdata USING btree (facility)`

func initRawdata(db *sql.DB) error {
	if _, err := db.Exec(sql_CreateTableRaw); err != nil {
		return err
	}
	if _, err := db.Exec(sql_CreateIndexRawFacility); err != nil {
		return err
	}
	return nil
}

func putRaw(db *sql.DB, facility, clientname string, trecv int64, data map[string]DataArr, body []byte) (int, error) {
	// data time range
	var tmin, tmax int64 = math.MaxInt64, -math.MaxInt64
	var t int64
	var isbad bool
	for _, points := range data {
		for i, point := range points {
			t = int64(point[0].(float64))
			if t > tmax {
				tmax = t
			}
			if t < tmin {
				tmin = t
			}

			// verify monotonicity
			if i > 0 && int64(point[0].(float64)) < t {
				isbad = true
			}
		}
	}
	// no data
	if tmax < 0 {
		return -1, nil
	}

	// store in table
	var id int
	sql := `INSERT INTO rawdata (facility, clientname, trecv, tdatamin, tdatamax, data)
		VALUES ($1, $2, $3, $4, $5, $6)
	RETURNING id`
	if err := db.QueryRow(sql, facility, clientname, trecv, tmin, tmax, body).Scan(&id); err != nil {
		return -1, fmt.Errorf("PutRaw: %v", err)
	}

	// non-monotonic data
	var err error
	if isbad {
		err = fmt.Errorf("Data not in time order")
	}

	return id, err
}

// GetRawsPG selects records from the raw table with (id > idFrom) or (tdata
// in a time window), unmarshals the containing records, and returns a result
// channel from which these can be received by a concurrent goproc.
func GetRawsPG(db *sql.DB, cancel <-chan struct{}, observatories []string, startTime, endTime *time.Time, idFrom uint64) (chan *RecordIn, int) {
	qfilter := ` WHERE facility = ANY($1)`
	if startTime != nil {
		qfilter += fmt.Sprintf(" AND tdatamax > %d", startTime.Unix())
	}
	if endTime != nil {
		qfilter += fmt.Sprintf(" AND tdatamin < %d", endTime.Unix())
	}
	if idFrom > 0 {
		qfilter += fmt.Sprintf(" AND id > %d", idFrom)
	}

	// do query
	var n int
	// filter by time or by id
	sql := `SELECT COUNT(*) FROM rawdata` + qfilter
	if err := db.QueryRow(sql, pq.Array(observatories)).Scan(&n); err != nil {
		log.Println(err)
		return nil, 0
	}
	sql = `SELECT id, facility, trecv, tdatamin, tdatamax, data FROM rawdata ` + qfilter +
		` ORDER BY tdatamax`
	rows, err := db.Query(sql, pq.Array(observatories))
	if err != nil {
		log.Println(err)
		return nil, 0
	}

	// prepare output channel
	out := make(chan *RecordIn, 10)

	// send records
	go func() {
		defer close(out)
		defer rows.Close()
		var t_, t0 time.Time
		var twait time.Duration // waiting for sync
		var n int
		var trecv int64
		var data []byte
		t0 = time.Now()
		for rows.Next() {
			rec := new(RecordIn)
			if err := rows.Scan(&rec.Id, &rec.Header.Facility, &trecv, &rec.Header.tDataMin, &rec.Header.tDataMax, &data); err != nil {
				log.Println("GetRawsPG: Scan:", err)
			}
			rec.Header.RecvTime = time.Unix(trecv, 0)
			if err := json.Unmarshal(data, &rec.Data); err != nil {
				log.Println("GetRawsPG: Unmarshal:", err)
			}

			t_ = time.Now()
			select {
			case out <- rec:
			case <-cancel:
				return
			}
			twait += time.Since(t_)
			n++
		}
		// print if took long
		if time.Since(t0).Seconds() > 1 {
			log.Printf("GetRawsPG: done: n_rec=%d t_tot=%.3fs t_wait=%.3fs obs=%v", n, time.Since(t0).Seconds(), twait.Seconds(), observatories)
		}
	}()

	return out, n
}

// mergeRecords consumes a GetRawsPG channel to create one big data record
// out of <nmax small ones.  Only the data part is merged, the Header is
// ignored.
// Data arrays in each incoming record are assumed to be ordered by time.
func mergeRecords(cancel chan struct{}, recs chan *RecordIn, nmax int) (ts map[string]DataArr, idmax uint64, doMore bool) {
	ts = make(map[string]DataArr)
	// loop over records
	var ok bool
	var rec *RecordIn
	var t_, t0 time.Time
	var twait time.Duration // waiting for sync
	var n int
	var n_merge, n_append int
	t0 = time.Now()
	for {
		t_ = time.Now()
		select {
		case rec, ok = <-recs:
		case <-cancel:
			return nil, idmax, true
		}
		twait += time.Since(t_)
		if !ok {
			break
		}

		if rec.Id > idmax {
			idmax = rec.Id
		}

		// parse body
		for k, v := range rec.Data {
			if _, ok := ts[k]; !ok {
				// don't assume initial capacity, we already consume faster than producing
				ts[k] = make(DataArr, len(v))
				copy(ts[k], v)
				continue
			}
			// records are received in order of tdatamax
			// the normal disjunct case can simply be appended
			if vlast := ts[k][len(ts[k])-1]; v[0][0].(float64) > vlast[0].(float64) {
				ts[k] = append(ts[k], v...)
				n_append += 1
			} else {
				ts[k] = ts[k].MergeFromRight(v)
				n_merge += 1
			}
		}

		// stop merging
		n += 1
		if n == nmax {
			break
		}
	}
	// print if took long
	if time.Since(t0).Seconds() > 1 {
		log.Printf("MergeRecords done: n_rec=%d n_var=%d id_max=%d n_merge=%d/%d t_tot=%.3fs t_wait=%.3fs\n", n, len(ts), idmax, n_merge, n_merge+n_append, time.Since(t0).Seconds(), twait.Seconds())
	}
	return ts, idmax, ok
}

// PutRawsBulkPG receives records from the recs channel and inserts them into
// the rawdata table after doing some cleanup:
// 1. It translates varnames to the new "_" delimiter from the old ":"
// 2. It skips data arrays that contain the UFT-8 "replacement character" as
//    this is not recognized as valid UFT-8 in postgres
// 3. It skips empty (heartbeat) records
func PutRawsBulkPG(db *sql.DB, cancel chan struct{}, recs chan *RecordIn, nrecs int) {
	var ok bool
	var rec *RecordIn
	var body []byte
	var err error
	var t_, t0 time.Time
	var twait time.Duration // waiting for sync
	var n, nn int
	var t, tmin, tmax int64
	var kfix string
	txn, _ := db.Begin()
	stmt, _ := txn.Prepare(pq.CopyIn("rawdata", "facility", "clientname", "trecv", "tdatamin", "tdatamax", "data"))

	log.Printf("PutRawsBulkPG: Start transfer: nrec=%d", nrecs)
	t0 = time.Now()
	for {
		t_ = time.Now()
		select {
		case rec, ok = <-recs:
		case <-cancel:
			return
		}
		twait += time.Since(t_)
		if !ok {
			break
		}
		n++

		// determine tData
		tmin, tmax = math.MaxInt64, -math.MaxInt64
		for _, points := range rec.Data {
			for _, point := range points {
				t = int64(point[0].(float64))
				if t > tmax {
					tmax = t
				}
				if t < tmin {
					tmin = t
				}
			}
		}
		// empty record
		if tmax < 0 {
			//log.Println("drop empty record:", rec.Id)
			continue
		}
		rec.Header.tDataMin = tmin
		rec.Header.tDataMax = tmax

		// fix missing tRecv
		if rec.Header.RecvTime.IsZero() {
			rec.Header.RecvTime = time.Unix(rec.Header.tDataMax, 0)
		}

		// rename variables
		// drop if data is invalid
		data := make(map[string]DataArr)
		for k, points := range rec.Data {
			// only check the first point
			if v, ok := points[0][1].(string); ok {
				// The UFT-8 "replacement character" is unsupported in postgres
				if strings.ContainsRune(v, '\ufffd') {
					log.Println("invalid rune in value:", rec.Id, k, v)
					continue
				}
			}
			// new name
			kfix = strings.Replace(k, ":", "_", -1)
			data[kfix] = points
		}

		// marshal body
		body, err = json.Marshal(data)
		if err != nil {
			log.Println("invalid json:", rec.Id, rec.Data, err)
			continue
		}

		// insert
		if _, err := stmt.Exec(rec.Header.Facility, rec.Header.Username, rec.Header.RecvTime.Unix(), rec.Header.tDataMin, rec.Header.tDataMax, body); err != nil {
			log.Println("stmt.Exec 1:", n, err, string(body))
			break
		}

		// flush
		nn++
		if nn == 1000000 {
			nn = 0
			log.Printf("PutRawsBulkPG: transfer batch: progress=%d/%d\n", n, nrecs)
			if _, err := stmt.Exec(); err != nil {
				log.Println("stmt.Exec buf:", n, err)
				break
			}
			if err := stmt.Close(); err != nil {
				log.Println("stmt.Close buf:", err)
				break
			}
			if err := txn.Commit(); err != nil {
				log.Println("stmt.Commit buf:", err)
				break
			}
			var err error
			txn, err = db.Begin()
			if err != nil {
				log.Println("Begin buf:", err)
				break
			}
			stmt, err = txn.Prepare(pq.CopyIn("rawdata", "facility", "clientname", "trecv", "tdatamin", "tdatamax", "data"))
			if err != nil {
				log.Println("Prepare buf:", err)
				break
			}
		}
	}
	if _, err := stmt.Exec(); err != nil {
		log.Println("stmt.Exec:", err)
	}
	if err := stmt.Close(); err != nil {
		log.Println("stmt.Close:", err)
	}
	if err := txn.Commit(); err != nil {
		log.Println("stmt.Commit:", err)
	}
	log.Printf("PutRawsBulkPG: recv done: nrec=%d t_tot=%.3fs t_wait=%.3fs\n", n, time.Since(t0).Seconds(), twait.Seconds())
}
