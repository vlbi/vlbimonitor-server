package userapi

import (
	"fmt"
	"net"
	"net/http"
)

// user exists
func (uc *UserCache) MinimalAuth(r *http.Request) (ok bool) {
	username, password, hasAuth := r.BasicAuth()

	if !hasAuth {
		return false
	}
	u, _ := uc.AuthenticateUser(username, password)
	return u != nil
}

// user is admin
func (uc *UserCache) AdminAuth(r *http.Request) (ok bool) {
	username, password, hasAuth := r.BasicAuth()

	if !hasAuth {
		return false
	}
	user, _ := uc.AuthenticateUser(username, password)
	if user == nil {
		return false
	}
	_, ok = uc.admins[user.Username]
	return ok
}

// extract the username+password from request
func (uc *UserCache) AuthenticateRequest(w http.ResponseWriter, r *http.Request) *User {
	username, password, hasAuth := r.BasicAuth()
	if !hasAuth {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		return nil
	}
	u, _ := uc.AuthenticateUser(username, password)
	return u
}

func (uc *UserCache) AuthenticateUser(username, password string) (*User, error) {
	// check username
	user := uc.Load(username)
	if user == nil {
		return nil, fmt.Errorf("Unknown username: '%s'", username)
	}

	// check password
	if passhash(password) != user.PasswordHash {
		return nil, fmt.Errorf("Invalid password: username='%s'", username)
	}

	return user, nil
}

// requestIp returns r.RemoteAddr if not loopback, otherwise X-Real-Ip, or nil on error
func requestIp(r *http.Request) net.IP {
	ip_, _, _ := net.SplitHostPort(r.RemoteAddr)
	ip := net.ParseIP(ip_)
	if ip != nil && !ip.IsLoopback() {
		return ip
	}
	// proxy forward
	ips, ok := r.Header["X-Real-Ip"]
	if !ok {
		return ip
	}
	return net.ParseIP(ips[0])
}

// authorize the request for this facility
func (uc *UserCache) AuthorizeProvider(r *http.Request, facility string) (string, string, error) {
	ip := requestIp(r)

	// validate BasicAuth against a username whitelist
	if _, _, hasAuth := r.BasicAuth(); hasAuth {
		if u, err := uc.AuthorizeUser(r, facility); err == nil {
			return u, ip.String(), err
		}
	}

	// validate IP address against a network whitelist
	clientname, err := uc.findNetwork(facility, ip)
	return clientname, ip.String(), err
}

func (uc *UserCache) AuthorizeUser(r *http.Request, facility string) (string, error) {
	// BasicAuth
	u, p, hasAuth := r.BasicAuth()
	if !hasAuth {
		return u, fmt.Errorf("Username/Password missing", facility)
	}
	if _, err := uc.AuthenticateUser(u, p); err != nil {
		//log.Println("AuthorizeUser:", err)
		return u, fmt.Errorf("Invalid Username/Password")
	}

	// Authorized for this facility
	if users, ok := uc.providers[facility]; !ok {
		return u, fmt.Errorf("No authorized users: facility='%s'", facility)
	} else if _, ok := users[u]; !ok {
		return u, fmt.Errorf("Unauthorized user: facility='%s' username='%s'", facility, u)
	}
	return u, nil
}

// findNetwork matches the current IP address against all networks defined for this facility
func (uc *UserCache) findNetwork(facility string, ip net.IP) (string, error) {
	for network, _ := range uc.providers[facility] {
		// undefined
		if len(network) == 0 {
			continue
		}
		if _, ipNet, err := net.ParseCIDR(network); err != nil {
			continue
		} else if ipNet.Contains(ip) {
			return ip.String(), nil
		}
	}
	return "", fmt.Errorf("IP address not whitelisted: %v", ip)
}
