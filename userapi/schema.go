package userapi

const schemaCreate = `{
	"properties": {
		"username": {
			"type": "string",
			"minLength": 3
		},
		"password": {
			"type": "string",
			"minLength": 3
		}
	},
	"additionalProperties": false
}`
