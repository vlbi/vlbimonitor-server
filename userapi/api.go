package userapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/apisupport"
	"database/sql"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/lib/pq"
	"log"
	"net/http"
	"strings"
)

// receiver type
type Api struct {
	*UserCache
}

// implement the Cruder interface
func (uc *Api) Attached(method string) bool {
	switch method {
	case "create":
		return true
	case "list":
		return true
	case "read":
		return true
	case "update":
		return true
	case "replace":
		return false
	case "delete":
		return true
	}
	return false
}

func (uc *Api) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// parse input
	var user User
	if err := apisupport.ParseJsonBody(r.Body, uc.schema, &user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// verify input
	msg := make([]string, 0)
	if len(user.Username) == 0 {
		msg = append(msg, "Username field is required")
	}
	if len(user.Password) == 0 {
		msg = append(msg, "Password field is required")
	}
	if len(msg) != 0 {
		http.Error(w, strings.Join(msg, "; "), http.StatusBadRequest)
		return
	}

	// insert user
	if err := uc.Insert(&user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(user.Username)
	log.Printf("Created User: '%s'", user.Username)
}

func (uc *Api) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	// filter parameters
	usernames, _ := r.Form["username"]

	// select all documents
	var rows *sql.Rows
	{
		var err error
		if usernames == nil {
			q := `SELECT username FROM users`
			rows, err = uc.db.Query(q)
		} else {
			q := `SELECT username FROM users WHERE username = ANY($1)`
			rows, err = uc.db.Query(q, pq.Array(usernames))
		}
		if err != nil {
			log.Println(err)
			return
		}
	}
	ids := make([]string, 0)
	var username string
	for rows.Next() {
		rows.Scan(&username)
		ids = append(ids, username)
	}
	json.NewEncoder(w).Encode(ids)
}

func (uc *Api) Read(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	user := uc.Load(id)
	if user == nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}
	json.NewEncoder(w).Encode(user)
}

func (uc *Api) Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	user := uc.Load(id)
	if user == nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	// parse input
	userNew := *user
	if err := apisupport.ParseJsonBody(r.Body, uc.schema, &userNew); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// insert user
	if err := uc.Reinsert(&userNew); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("Updated User: '%s'", user.Username)
}

func (uc *Api) Replace(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (uc *Api) Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	err := uc.Drop(id)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}
	json.NewEncoder(w).Encode("user deleted: " + id)
	log.Printf("Deleted User: '%s'", id)
}
