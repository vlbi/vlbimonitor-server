package userapi

import (
	"crypto/md5"
	"database/sql"
	"github.com/xeipuuv/gojsonschema"
	"log"
	"sync"
	"time"
)

// password is hashed
const sql_CreateTable string = `CREATE TABLE IF NOT EXISTS users (
	username TEXT PRIMARY KEY,
	password BYTEA NOT NULL,
	email TEXT,
	creationtime INTEGER NOT NULL,
	updatetime INTEGER NOT NULL,
	isexpired BOOLEAN DEFAULT FALSE
)`

// generator
func NewUserCache(db *sql.DB, adminarr []string, authorizedProviders map[string][]string) *UserCache {
	// load schema
	loader := gojsonschema.NewStringLoader(schemaCreate)
	schema, err := gojsonschema.NewSchema(loader)
	if err != nil {
		log.Panic(err)
	}

	admins := make(map[string]struct{})
	for _, user := range adminarr {
		admins[user] = struct{}{}
	}

	providers := make(map[string]map[string]struct{})
	for k, v := range authorizedProviders {
		providers[k] = make(map[string]struct{})
		for _, vv := range v {
			providers[k][vv] = struct{}{}
		}
	}

	uc := UserCache{
		db:        db,
		cache:     make(map[string]*User),
		schema:    schema,
		admins:    admins,
		providers: providers,
	}

	if db != nil {
		if _, err := db.Exec(sql_CreateTable); err != nil {
			log.Fatalf("userapi: %v", err)
		}
	}

	// insert admin user if not exists
	admin := uc.Load("admin")
	if admin == nil {
		log.Println("create admin user")
		admin = &User{Username: "admin", Password: "admin"}
		uc.Insert(admin)
	}

	return &uc
}

// receiver type
type UserCache struct {
	db        *sql.DB
	cache     map[string]*User
	lock      sync.RWMutex
	schema    *gojsonschema.Schema
	admins    map[string]struct{}
	providers map[string]map[string]struct{}
}
type User struct {
	Username     string
	Password     string   `json:",omitempty" bson:"-"`
	PasswordHash [16]byte `json:"-"`
	CreationTime int64
	UpdateTime   int64
}

// insert a new user
func (uc *UserCache) Insert(user *User) error {
	now := time.Now().Unix()

	// fill fields
	user.PasswordHash = passhash(user.Password)
	user.Password = ""
	user.CreationTime = now
	user.UpdateTime = now

	// insert user in PG
	sql := `INSERT INTO users (username, password, creationtime, updatetime)
		VALUES ($1, $2, $3, $4)
	ON CONFLICT (username)
	DO NOTHING`
	passwordHash := []byte(user.PasswordHash[:])
	uc.db.Exec(sql, user.Username, passwordHash, now, now)

	uc.lock.Lock()
	uc.cache[user.Username] = user
	uc.lock.Unlock()

	return nil
}

func (uc *UserCache) Reinsert(user *User) error {
	now := time.Now().Unix()

	// fill fields
	if user.Password != "" {
		user.PasswordHash = passhash(user.Password)
		user.Password = ""
	}
	user.UpdateTime = now

	// reinsert user in PG
	sql := `INSERT INTO users (username, password, creationtime, updatetime)
		VALUES ($1, $2, $3, $4)
	ON CONFLICT (username)
	DO UPDATE
		SET username = $1, password = $2, updatetime = $3, creationtime = $4`
	passwordHash := []byte(user.PasswordHash[:])
	if _, err := uc.db.Exec(sql, user.Username, passwordHash, user.CreationTime, now); err != nil {
		log.Println("user reinsert:", err)
	}

	uc.lock.Lock()
	uc.cache[user.Username] = user
	uc.lock.Unlock()

	return nil
}

func (uc *UserCache) Load(username string) *User {
	// from cache
	uc.lock.RLock()
	if user, ok := uc.cache[username]; ok {
		uc.lock.RUnlock()
		return user
	}
	uc.lock.RUnlock()

	// find user in PG
	var u User
	sql := `SELECT username, password, creationtime, updatetime FROM users WHERE username = $1`
	var passwordHash []byte = []byte{1}
	if err := uc.db.QueryRow(sql, username).Scan(&u.Username, &passwordHash, &u.CreationTime, &u.UpdateTime); err != nil {
		//log.Printf("user load '%s': %v", username, err)
		return nil
	}
	copy(u.PasswordHash[:], passwordHash)

	// into cache
	uc.lock.Lock()
	uc.cache[username] = &u
	uc.lock.Unlock()

	return &u
}

// drop user from cache and db
func (uc *UserCache) Drop(username string) error {
	uc.lock.Lock()
	delete(uc.cache, username)
	uc.lock.Unlock()

	// drop user in PG
	sql := `DELETE FROM users WHERE username = $1`
	if _, err := uc.db.Exec(sql, username); err != nil {
		log.Println("user delete:", err)
		return err
	}
	return nil
}

// helper functions
func passhash(password string) [16]byte {
	return md5.Sum([]byte(password))
}
