package vexparser

/*
A Vexfile consists of three types of declarations: 1. named Sections 2. named
Paragraphs 3. named Values

A Vexfile is line-based; a complete line can be a commented out by a leading '*'
character.

One line can contain multiple declarations; each declaration ends with a ';'
character.

Declarations can span multiple lines, i.e. a number of unterminated lines
followed by a terminated line.

A Section declaration start with a '$' symbol.

A paragraph declaration start with identifier 'def' or 'scan' and ends with
identifier 'enddef' or 'endscan', respectively.

Section and Paragraph names are unique, Value names may be repeated.

A Vexfile is loaded into nested objects in which Sections and Paragraphs are
objects themselves.  Each nesting level in the object (Vexfile, Section, or
Paragraph) can contain named Values (key-value pairs).  The value in a key-value
pair is of type 'string' (in single-value cases) or of type 'array' (in
multi-value cases).

(A JS implementation of this parser is used client side.)
*/

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"hash/crc32"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var REQUIRED_VEX_FIELDS = []string{"$GLOBAL", "$SCHED", "filename", "checksum"}
var REQUIRED_GLOBAL_FIELDS = []string{"exper_name", "exper_nominal_start", "exper_nominal_stop"}
var REQUIRED_SCHED_SCAN_FIELDS = []string{"start", "source", "station"}

type Object map[string]interface{}

func Parse(canonical, filename string) Object {
	fh, _ := os.Open(canonical)
	defer fh.Close()
	reader := bufio.NewReader(fh)

	var hash string

	stack := make([]Object, 0, 3) // catch the case where length > expected maximum length of 2
	obj := make(Object)
	obj["filename"] = filename

	var err error
	var iline int = 0
	var k, v, line, name string
	var prev, pardef string = "", ""
	var sublines, words, arr []string
	var warn = func(msg string) {
		log.Printf("parse error: %s(%d): %s", filename, iline, msg)
	}

	for {
		line, err = reader.ReadString('\n')
		if err != nil {
			break
		}
		iline += 1
		// handle potential msdos CRLF file format
		line = strings.TrimRight(line, "\n")
		line = strings.TrimRight(line, "\r")
		if len(line) == 0 { // empty
			continue
		}
		if line[0] == '*' { // comment
			continue
		}
		// line continuation
		line = prev + strings.TrimSpace(line)
		prev = ""

		sublines = strings.Split(line, ";")
		for j := 0; j < len(sublines); j++ {
			line = strings.TrimSpace(sublines[j])
			if len(line) == 0 { // empty
				continue
			}
			if line[0] == '*' { // comment subline
				continue
			}
			// continued line, otherwise the last subline would have length 0
			if j == len(sublines)-1 {
				prev = line
				break
			}
			hash += line
			// section declaration
			if line[0] == '$' {
				name = line
				if len(stack) == 0 {
					stack = append(stack, obj)
				}
				if len(stack) > 1 {
					warn(fmt.Sprintf("sec: stacklevel %d, should be 2", len(stack)))
					stack = stack[:1]
				}
				stack[0][name] = make(Object)
				obj = stack[0][name].(Object)
				// paragraph definition identifier
				pardef = "def"
				if name == "$SCHED" {
					pardef = "scan"
				}
				continue
			}
			// paragraph declaration
			if line[0:len(pardef)+1] == pardef+" " {
				name = line[len(pardef)+1:]
				if len(stack) == 1 {
					stack = append(stack, obj)
				} else {
					warn(fmt.Sprintf("par: stacklevel %d, should be 1", len(stack)))
				}
				obj[name] = make(Object)
				obj = obj[name].(Object)
				continue
			}
			if line == "end"+pardef {
				if len(stack) == 2 {
					obj, stack = stack[len(stack)-1], stack[:len(stack)-1]
				} else {
					warn(fmt.Sprintf("end%s: stacklevel %d, should be 2", pardef, len(stack)))
				}
				continue
			}

			// parameter declaration
			words = strings.Split(line, "=")
			if len(words) != 2 {
				warn(fmt.Sprintf("invalid parameter declaration%s", line))
				continue
			}
			k = strings.TrimSpace(words[0])
			v = strings.TrimSpace(words[1])
			if _, in := obj[k]; !in {
				obj[k] = v
			} else { // append to array
				switch obj[k].(type) {
				case string:
					arr = make([]string, 1, 2)
					arr[0] = obj[k].(string)
				case []string:
					arr = obj[k].([]string)
				}
				arr = append(arr, v)
				obj[k] = arr
			}
		}
	}
	// last paragraph was not terminated, it may be incomplete
	if len(stack) > 1 {
		log.Println("last paragraph unterminated, drop it:", filename, name)
		delete(obj, name)
	}
	// incomplete input will leave the stack empty
	if len(stack) > 0 {
		obj = stack[0]
	}

	// checksum
	checksum := crc32.ChecksumIEEE([]byte(hash))
	obj["checksum"] = checksum

	h := md5.New()
	fh.Seek(0, 0)
	io.Copy(h, fh)
	obj["md5sum"] = fmt.Sprintf("%x", h.Sum(nil))

	return obj
}

func (vex Object) Expand(o Object) {
	for k, v := range o {
		switch v.(type) {
		case Object:
			vex.Expand(v.(Object))
			continue
		case string:
		default:
			continue
		}
		// is this a reference key?
		if len(k) <= 4 {
			continue
		}
		if k[0:4] != "ref " {
			continue
		}
		ref := k[4:]
		// ref section does not exist
		if _, ok := vex[ref]; !ok {
			continue
		}
		refO := vex[ref].(Object)
		// paragraph does not exist in ref section
		sec, ok := refO[v.(string)]
		if !ok {
			continue
		}
		// referred paragraph is not an object
		secO, ok := sec.(Object)
		if !ok {
			continue
		}
		// replace reference with contents of object it refers to
		delete(o, k)
		for kk, vv := range secO {
			o[kk] = vv
		}
	}
}

// sanity check
func (vex Object) Sanity() error {
	// sections
	for _, k := range REQUIRED_VEX_FIELDS {
		if _, ok := vex[k]; !ok {
			return fmt.Errorf("no field '%s' in vex", k)
		}
	}

	// valid $GLOBAL.'ref $EXPER'
	globalO := vex["$GLOBAL"].(Object)
	for _, k := range REQUIRED_GLOBAL_FIELDS {
		if _, ok := globalO[k]; !ok {
			return fmt.Errorf("no field '%s' in vex", k)
		}
	}

	// paragraph contents
	schedO := vex["$SCHED"].(Object)
	for scanname, scan := range schedO {
		scanO := scan.(Object)
		for _, k := range REQUIRED_SCHED_SCAN_FIELDS {
			if _, ok := scanO[k]; !ok {
				return fmt.Errorf("no field '%s' in scan '%s'", k, scanname)
			}
		}
	}
	return nil
}

func ToTime(vextime string) time.Time {
	r := regexp.MustCompile("[ydhms]")
	words := r.Split(vextime, 6)

	var ta [5]int
	for i, w := range words[:len(words)-1] { // last element is empty
		t_, _ := strconv.ParseInt(w, 10, 64)
		ta[i] = int(t_)
	}

	return time.Date(ta[0], time.January, ta[1], ta[2], ta[3], ta[4], 0, time.UTC)
}

func (scan Object) Sites() []string {
	station := scan["station"]

	var stations []string
	switch v := station.(type) {
	case string:
		stations = []string{v}
	case []string:
		stations = v
	}

	sites := make([]string, 0)
	for _, station := range stations {
		words := strings.Split(station, ":")
		if len(words) < 1 {
			continue
		}
		k := strings.TrimSpace(words[0])
		sites = append(sites, k)
	}
	return sites
}
