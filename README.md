Radboud Radio Lab VLBI Monitor
==============================

A web server with MongoDB backend for monitoring Very Long Baseline Interferometry campaigns.

License
-------

*  The server is available under the terms of the [GNU Affero General Public
   License](http://www.gnu.org/licenses/agpl-3.0.html) as published by the
   Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

*  All (example) clients are available under the terms of the [MIT License]
   (http://opensource.org/licenses/MIT).


Installation
---------------------------------
*  Make sure you have a working [installation of Go](https://golang.org/doc/install)
   and your `$GOROOT` is set appropriately.

*  Download and install the VLBI Monitor and all required dependencies.

        $ go get bitbucket.org/vlbi/vlbimonitor-server


Initial Configuration
---------------------------------
*  Create a [MongoDB](https://www.mongodb.org) database.

        $ mongo
        > use vlbi_monitor
        > CTRL + D

* Create a PG database

	$ sudo -iu postgres
	$ createuser vlbimonitor
	$ createdb vlbimonitor
        $ CTRL + D

*  Clone the web interface

        $ git clone https://bitbucket.org/vlbi/vlbimonitor-webinterface.git $HOME/vlbimonitor-webinterface

*  Clone and initialize the masterlists

        $ git clone https://bitbucket.org/vlbi/vlbimonitor-masterlists.git $HOME/vlbimonitor-masterlists
        $ cd $HOME/vlbimonitor-masterlists
        $ ./tools/gen-json-schema.sh masterlist.csv > schema.json

*  Copy the sample configuration file to the default location and adapt the settings.

        $ cp $GOPATH/src/bitbucket.org/vlbi/vlbimonitor-server/vlbimonitor.conf $HOME/.config/vlbimonitor/conf.json
        $ vi $HOME/.config/vlbimonitor/conf.json

Usage
---------------------------------
Start the web server.

        $ vlbimonitor &
	
This uses the conf.json in the default location (see above).
A different configuration file can optionally be specified on the command line.

        $ vlbimonitor $HOME/test.json &
   
If the web server exits with the message "no reachable servers", verify that
the _DatabaseAddress_ is configured to the correct address and port in `conf.json`.
The default port for mongod instances (as of version 3.2.6) is 27017.

Configuring access
---------------------------------

*  Use the client tools to change the administrator password.
   The default password is "admin".

        $ python adduser.py -c admin

*  Add a user with permissions for one or more facilities.

        $ python adduser.py username -f APEX ALMA

   At least one user per facility is needed. But multiple users
   may have access to a facility and a single user may have
   access to multiple facilities.

Test
---------------------------------
*  Connect to server URL as configured in `conf.json`
   (default is `http://localhost:8000).
