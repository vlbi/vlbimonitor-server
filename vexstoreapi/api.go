package vexstoreapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/vexparser"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"path"
	"strings"
)

// generator
func NewVexstore(dir string) *Vexstore {
	return &Vexstore{dir: dir}
}

type Vexstore struct {
	dir string
}
type VexstoreFile struct {
	Filename  string `json:"filename"`
	IsDir     bool   `json:"isDir"`
	ModTime   int64  `json:"modTime"` // support incremental updates
	Md5sum    string `json:"md5sum"`
	ExperName string `json:"experName"`
	StartTime int64  `json:"startTime"`
	StopTime  int64  `json:"stopTime"`
}

func (vs *Vexstore) List(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	type object = vexparser.Object

	dir := path.Join(vs.dir, ps.ByName("path"))

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	list := make([]*VexstoreFile, 0, len(files))
	for _, f := range files {
		vf := VexstoreFile{
			Filename: f.Name(),
			IsDir:    f.IsDir(),
			ModTime:  f.ModTime().Unix(),
		}
		list = append(list, &vf)

		// not a vexfile
		if f.IsDir() || !strings.HasSuffix(f.Name(), ".vex") {
			continue
		}

		// parse vex file
		vex := vexparser.Parse(dir+f.Name(), f.Name())
		vex.Expand(vex)
		if err := vex.Sanity(); err != nil {
			continue
		}

		// insert additional info
		vf.Md5sum = vex["md5sum"].(string)
		vf.ExperName = vex["$GLOBAL"].(object)["exper_name"].(string)
		v := vex["$GLOBAL"].(object)["exper_nominal_start"].(string)
		vf.StartTime = vexparser.ToTime(v).Unix()
		v = vex["$GLOBAL"].(object)["exper_nominal_stop"].(string)
		vf.StopTime = vexparser.ToTime(v).Unix()
	}

	json.NewEncoder(w).Encode(list)
}
