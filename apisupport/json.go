package apisupport

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/xeipuuv/gojsonschema"
	"io"
	"strings"
)

// verify input against json schema and unmarshal
func ParseJsonBody(body io.ReadCloser, schema *gojsonschema.Schema, v interface{}) error {
	// read from io.ReadCloser
	buf := new(bytes.Buffer)
	buf.ReadFrom(body)

	// validate input
	documentLoader := gojsonschema.NewStringLoader(buf.String())
	result, err := schema.Validate(documentLoader)
	if err != nil {
		return err
	}
	if !result.Valid() {
		var msgs []string
		for _, e := range result.Errors() {
			msgs = append(msgs, e.String())
		}
		return fmt.Errorf(strings.Join(msgs, "\n"))
	}

	// decode input
	if err := json.Unmarshal(buf.Bytes(), v); err != nil {
		return err
	}
	return nil
}
