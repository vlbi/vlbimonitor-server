package obslogapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/userapi"
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"log"
)

const sql_CreateTableObsLog string = `CREATE TABLE IF NOT EXISTS obslog (
	id BIGSERIAL NOT NULL,
	source TEXT NOT NULL,
	facility TEXT NOT NULL,
	username TEXT NOT NULL,
	scanid TEXT NOT NULL,
	vexid TEXT NOT NULL,
	posttime INTEGER NOT NULL,
	applytime INTEGER NOT NULL,
	type TEXT NOT NULL,
	content TEXT NOT NULL
);
DO $$
begin
	IF NOT EXISTS (SELECT * FROM information_schema.constraint_column_usage WHERE constraint_name='obslog_pkey') THEN 
		ALTER TABLE ONLY obslog ADD CONSTRAINT "obslog_pkey" PRIMARY KEY (id,source);
	end if;
END $$;`

type ObsLogStore struct {
	db              *sql.DB
	users           *userapi.UserCache
	messageSourceId string
}

type LogEntry struct {
	Id        int64
	Source    string
	Facility  string
	User      string
	ScanId    string
	VexId     string
	PostTime  int64
	ApplyTime int64
	Type      string
	Content   string
}

func NewObsLogStore(db *sql.DB, users *userapi.UserCache, messageSourceId string) *ObsLogStore {
	store := ObsLogStore{
		db:              db,
		users:           users,
		messageSourceId: messageSourceId}

	if db != nil {
		if _, err := db.Exec(sql_CreateTableObsLog); err != nil {
			log.Fatalf("obslogapi: create table: %v", err)
		}
	}

	return &store
}

func (s *ObsLogStore) AddLogEntry(entry *LogEntry) error {
	var id int
	sql := `INSERT INTO obslog (source, facility, username, scanid, vexid, posttime, applytime, type, content) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id`
	if err := s.db.QueryRow(sql, entry.Source, entry.Facility, entry.User, entry.ScanId, entry.VexId, entry.PostTime, entry.ApplyTime, entry.Type, entry.Content).Scan(&id); err != nil {
		return errors.Wrap(err, "new obslog message insert failed")
	}
	return nil
}

func (s *ObsLogStore) GetLogEntriesByVex(vexId, facility string, lastIds map[string]int64) ([]LogEntry, error) {
	res := make([]LogEntry, 0)

	// prep a list of source id's
	sources := make([]string, 0)
	for key, _ := range lastIds {
		sources = append(sources, key)
	}

	// build sql query and parameter list
	var sql string
	params := make([]interface{}, 0)

	if facility != "" {
		sql = `SELECT * FROM obslog WHERE facility=$1 AND vexid=$2 AND ((source <> ALL($3))` // at this point: 1 open (
		params = append(params, facility)
	} else {
		sql = `SELECT * FROM obslog WHERE vexid=$1 AND ((source <> ALL($2))` // at this point: 1 open (
	}

	params = append(params, vexId)
	params = append(params, pq.Array(sources))

	for source, lastId := range lastIds {
		sql += fmt.Sprintf(` OR (source = $%d AND id > %d)`, len(params)+1, lastId)
		params = append(params, source)
	}
	sql += `)` // close the last bracket

	log.Printf("sql query: %+v", sql)
	log.Printf("extra params: %+v", params)

	// execute query
	rows, err := s.db.Query(sql, params...)
	if err != nil {
		return res, errors.Wrap(err, "error in query for obslog entries")
	}
	defer rows.Close() // good practice, though rows are unconditionally consumed below

	// process result
	for rows.Next() {
		var e LogEntry
		rows.Scan(&e.Id, &e.Source, &e.Facility, &e.User, &e.ScanId, &e.VexId, &e.PostTime, &e.ApplyTime, &e.Type, &e.Content)
		res = append(res, e)
	}

	return res, nil
}
