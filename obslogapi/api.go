package obslogapi

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/pkg/errors"
	"net/http"
	"strconv"
	"time"
)

type Api struct {
	*ObsLogStore
}

func (sc *Api) Attached(method string) bool {
	switch method {
	case "create":
		return true
	case "list":
		return true
	default:
		return false
	}
}

func (a *Api) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	user := a.users.AuthenticateRequest(w, r)
	if user == nil {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	now := time.Now().Unix()
	logtime, err := strconv.ParseInt(r.PostFormValue("applytime"), 10, 64)
	if err != nil {
		http.Error(w, errors.Wrap(err, "time is not an int").Error(), http.StatusInternalServerError)
		return
	}
	entry := LogEntry{
		Source:    a.messageSourceId,
		Facility:  r.PostFormValue("facility"),
		User:      user.Username,
		ScanId:    r.PostFormValue("scanid"),
		VexId:     r.PostFormValue("vexid"),
		PostTime:  now,
		ApplyTime: logtime,
		Type:      r.PostFormValue("type"),
		Content:   r.PostFormValue("content")}

	if err := a.AddLogEntry(&entry); err != nil {
		http.Error(w, errors.Wrap(err, "log entry insert failed").Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(struct {
		Id     int64
		source string
	}{entry.Id, entry.Source})

}

func (a *Api) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// decode url parameters
	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	facility := r.FormValue("facility")
	vexId := r.FormValue("vexid")
	last := r.FormValue("last")

	// decode lastIds from json to map
	lastIds := make(map[string]int64)
	if err := json.Unmarshal([]byte(last), &lastIds); err != nil {
		http.Error(w, errors.Wrap(err, "could not decode last ids: "+last).Error(), http.StatusInternalServerError)
		return
	}

	// retrieve messages
	msgs, err := a.GetLogEntriesByVex(vexId, facility, lastIds)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// encode result as json
	json.NewEncoder(w).Encode(msgs)
}

func (a *Api) Read(w http.ResponseWriter, r *http.Request, _ httprouter.Params)    {}
func (a *Api) Replace(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {}
func (a *Api) Delete(w http.ResponseWriter, r *http.Request, _ httprouter.Params)  {}
func (a *Api) Update(w http.ResponseWriter, r *http.Request, _ httprouter.Params)  {}
