SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: facilities; Type: TABLE; Schema: public; Owner: vlbimonitor
--

CREATE TABLE public.facilities (
    facilityid integer NOT NULL,
    longname text NOT NULL,
    abbreviation text NOT NULL,
    isobservatory boolean
);


ALTER TABLE public.facilities OWNER TO vlbimonitor;

--
-- Name: facilities_facilityid_seq; Type: SEQUENCE; Schema: public; Owner: vlbimonitor
--

CREATE SEQUENCE public.facilities_facilityid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.facilities_facilityid_seq OWNER TO vlbimonitor;

--
-- Name: facilities_facilityid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vlbimonitor
--

ALTER SEQUENCE public.facilities_facilityid_seq OWNED BY public.facilities.facilityid;


--
-- Name: rawdata; Type: TABLE; Schema: public; Owner: vlbimonitor
--

CREATE TABLE public.rawdata (
    rawdataid integer NOT NULL,
    userid integer,
    facilityid integer,
    recvtime time(6) without time zone,
    sendtime time(6) without time zone,
    packetnumber integer,
    clientversion text,
    data jsonb
);


ALTER TABLE public.rawdata OWNER TO vlbimonitor;

--
-- Name: rawdata_rawdataid_seq; Type: SEQUENCE; Schema: public; Owner: vlbimonitor
--

CREATE SEQUENCE public.rawdata_rawdataid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rawdata_rawdataid_seq OWNER TO vlbimonitor;

--
-- Name: rawdata_rawdataid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vlbimonitor
--

ALTER SEQUENCE public.rawdata_rawdataid_seq OWNED BY public.rawdata.rawdataid;



--
-- Name: users; Type: TABLE; Schema: public; Owner: vlbimonitor
--

CREATE TABLE public.users (
    userid integer NOT NULL,
    realname text NOT NULL,
    homefacility integer NOT NULL,
    email text NOT NULL,
    passwordhash text NOT NULL,
    isadmin boolean NOT NULL
);


ALTER TABLE public.users OWNER TO vlbimonitor;

--
-- Name: users_userid_seq; Type: SEQUENCE; Schema: public; Owner: vlbimonitor
--

CREATE SEQUENCE public.users_userid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userid_seq OWNER TO vlbimonitor;

--
-- Name: users_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vlbimonitor
--

ALTER SEQUENCE public.users_userid_seq OWNED BY public.users.userid;


--
-- Name: facilities facilityid; Type: DEFAULT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.facilities ALTER COLUMN facilityid SET DEFAULT nextval('public.facilities_facilityid_seq'::regclass);


--
-- Name: rawdata rawdataid; Type: DEFAULT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.rawdata ALTER COLUMN rawdataid SET DEFAULT nextval('public.rawdata_rawdataid_seq'::regclass);


--
-- Name: users userid; Type: DEFAULT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.users ALTER COLUMN userid SET DEFAULT nextval('public.users_userid_seq'::regclass);


--
-- Name: facilities facilities_pkey; Type: CONSTRAINT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.facilities
    ADD CONSTRAINT facilities_pkey PRIMARY KEY (facilityid);


--
-- Name: rawdata rawdata_pkey; Type: CONSTRAINT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.rawdata
    ADD CONSTRAINT rawdata_pkey PRIMARY KEY (rawdataid);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


--
-- Name: users facility_fkey; Type: FK CONSTRAINT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT facility_fkey FOREIGN KEY (homefacility) REFERENCES public.facilities(facilityid);


--
-- Name: rawdata facility_fkey; Type: FK CONSTRAINT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.rawdata
    ADD CONSTRAINT facility_fkey FOREIGN KEY (facilityid) REFERENCES public.facilities(facilityid);


--
-- Name: rawdata user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: vlbimonitor
--

ALTER TABLE ONLY public.rawdata
    ADD CONSTRAINT user_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);


--
-- PostgreSQL database dump complete
--

