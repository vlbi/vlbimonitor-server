#!/bin/env python3

from pymongo import MongoClient
import psycopg2
import json
from pprint import pprint, pformat
import time
from dateutil import parser
import datetime
import sys
from PyQt5.QtCore import QObject, QCoreApplication, QThread, QThreadPool, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QGridLayout, QProgressBar, QLabel, QSpacerItem, QSizePolicy, QMainWindow

facilities = ['LMT', 'PICO', 'SMTO', 'GLT', 'NOEMA', 'APEX', 'SMA', 'KP', 'SPT', 'ALMA', 'JCMT']
#facilities = ['PICO',  'KP', 'SPT', 'ALMA']


class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.workerThread = QThread()
        self.worker = Worker()
        self.worker.moveToThread(self.workerThread)
        self.worker.finished.connect(self.workerThread.quit)
        self.worker.progress.connect(self.updateui)
        self.workerThread.started.connect(self.worker.run)
        #self.workerThread.finished.connect(QCoreApplication.instance().quit)
        self.workerThread.start()


    def initUI(self):
        main = QWidget()
        layout = QGridLayout()
        self.bars = {}
        self.skiplabels = {}
        for i,f in enumerate(facilities):
            layout.addWidget(QLabel(f), i, 0)
            self.bars[f] = QProgressBar()
            layout.addWidget(self.bars[f], i, 1)
            self.skiplabels[f] = QLabel("-")
            layout.addWidget(self.skiplabels[f], i, 2)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.dateLabel = QLabel('-')
        layout.addWidget(self.dateLabel, len(facilities), 0, 1, 3)
        layout.addItem(verticalSpacer)
        main.setLayout(layout)
        self.setCentralWidget(main)
        self.show()

    def updateui(self, facility, done, total, skipped, date):
        self.bars[facility].setValue(100.0*done/total)
        self.bars[facility].setFormat('{}/{}'.format(done, total))
        self.skiplabels[facility].setText('{}'.format(skipped))
        self.dateLabel.setText(date)






class Worker(QObject):
    progress = pyqtSignal(str, int, int, int , str)
    finished = pyqtSignal()
    totals = {}
    counts = {f:0 for f in facilities}
    cursors = {}
    skipped = {f:0 for f in facilities}
    next = {f:None for f in facilities}

    def advance(self, facility):
        while True:
            try:
                doc = self.cursors[facility].next()
                self.counts[facility] = self.counts[facility]+1
            except StopIteration:
                self.next[facility] = None
                return
            if ('data' not in doc) or ('header' not in doc) or ('sendTime' not in doc['header']) or ('recvTime' not in doc['header']):
                self.skipped[facility] = self.skipped[facility]+1
                continue
            self.next[facility] = doc
            return

    def run(self):
        # connect to mongo
        mgo = MongoClient('localhost',27017)
        db = mgo.vlbi_monitor

        # connect to postgres
        conn = psycopg2.connect(host="localhost",database="vlbimonitor", user="vlbimonitor", password="")
        cur = conn.cursor()

        for f in facilities:
            print("prepping for {}".format(f))
            self.totals[f]  = db['raw_{}'.format(f)].estimated_document_count()
            self.cursors[f] = db['raw_{}'.format(f)].find().sort('header.recvTime')
            self.advance(f)

        while True:
            # determine which facility has the earlier next item
            nextt = datetime.datetime(9999,1,1,0,0,0)
            nextf = None
            nextd = None
            #for f in facilities:
            #    print("{:5s}: {}".format(f, next[f]['header']['recvTime']))
            for f in facilities:
                if self.next[f] is None:
                    continue
                if self.next[f]['header']['recvTime'] < nextt:
                    nextd = self.next[f]
                    nextt = self.next[f]['header']['recvTime']
                    nextf = f
            
            if nextf is None:
                break # we're done!

            # process this item
            tdatamin = 999999999999999999
            tdatamax = 0
            for vardata in nextd['data'].values():
                for sample in vardata:
                    tdatamin = min(tdatamin, sample[0])
                    tdatamax = max(tdatamax, sample[0])
            cur.execute("INSERT INTO rawdata (facility, trecv, clientname, tdatamin, tdatamax, data) VALUES (%s,%s,%s,%s, %s, %s)", [nextf, int(nextd['header']['sendTime'].timestamp()), 'mongo2sql.py', tdatamin, tdatamax, json.dumps(nextd['data']) ])
    
            # pop a new item for this facility
            self.advance(nextf)
            #self.bars[nextf].setValue(100*counts[nextf]/totals[nextf])
            #for f in facilities:
            #    skiplabels[f].setText('{}'.format(skipped[f]))
            #dateLabel.setText('{}'.format(nextt))
            self.progress.emit(nextf, self.counts[nextf], self.totals[nextf], self.skipped[nextf], '{}'.format(nextt))
            #print("{}: {:5s}: {:.2f}%".format(nextt, nextf, 100.0*counts[nextf]/totals[nextf]))
        conn.commit()
        #qCoreApplication.instance().quit()
        self.finished.emit()


#def update_gui(facility, num):
#    bars[facility].setValue(100*num/totals[facility])

app = QApplication([])
m = Main()
sys.exit(app.exec_())

