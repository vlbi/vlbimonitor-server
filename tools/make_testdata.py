import psycopg2
import numpy as np
import json
np.random.seed(42)

conn = psycopg2.connect(host="localhost",database="vlbimonitor", user="vlbimonitor", password="")
cur = conn.cursor()

# clean up old data
cur.execute("TRUNCATE TABLE rawdata;")
cur.execute("TRUNCATE TABLE timeseries;")
cur.execute('ALTER SEQUENCE "rawdata_recordId_seq" restart;') # double quotes mandatory because of the capital I
cur.execute('ALTER SEQUENCE "timeseries_timeseriesid_seq" restart;')
cur.execute("UPDATE state SET val=to_json(0) WHERE key='last-raw-id';")
cur.execute("DELETE FROM state WHERE key != 'last-raw-id';")

# ideal test data contains multiple points per raw. not too many data point but at least sufficient to fill multiple timeseries records. Data values will be sequential
time = 1563000000
seq  = 0
for i in range(500):
    datapoints = []
    for r in range(np.random.randint(1,5)):
        time += np.random.randint(1,5)
        if np.random.rand() < 0.01:
            time += 60*60*24
        seq += 1
        datapoints.append([time,seq])
    data = {'weather_tau225': datapoints}
    tdatamin = min([x[0] for x in datapoints])
    tdatamax = max([x[0] for x in datapoints])
    cur.execute("INSERT INTO rawdata (facility, trecv, data, clientname, tdatamin, tdatamax) VALUES ('PICO', %s, %s, 'test', %s, %s);", [time, json.dumps(data), tdatamin, tdatamax])
    
  
# now insert some 'messy' records
datapoints = [
    [1563000000+60, 'foo_1'],
    [1563000000+61, 'foo_2'],
    [1563000000+62, 'foo_3']
]
data = {'weather_tau225': datapoints}
tdatamin = min([x[0] for x in datapoints])
tdatamax = max([x[0] for x in datapoints])
cur.execute("INSERT INTO rawdata (facility, trecv, data, clientname, tdatamin, tdatamax) VALUES ('PICO', %s, %s, 'test', %s, %s)", [tdatamax, json.dumps(data), tdatamin, tdatamax])

conn.commit()
