import psycopg2
import numpy as np
import json
np.random.seed(42)

conn = psycopg2.connect(host="localhost",database="vlbimonitor", user="vlbimonitor", password="")
cur = conn.cursor()

# clean up old data
cur.execute("TRUNCATE TABLE timeseries;")
cur.execute('ALTER SEQUENCE "timeseries_timeseriesid_seq" restart;')
cur.execute("UPDATE state SET val=to_json(0) WHERE key='last-raw-id';")

conn.commit()
