package main

import (
	"gopkg.in/yaml.v2"
	"log"
	"os"
)

type ServerConfig struct {
	PostgresInfo struct {
		Host     string
		Port     int
		User     string
		Pass     string
		Database string
	} `yaml:"postgres"`

	ServerAddress string `yaml:"serverAddress"`
	RecordSchema  string `yaml:"recordSchema"`

	MessageSourceId  string `yaml:"messageSourceId"`
	LocalObservatory string `yaml:"localObservatory"`
	Observatories    []string

	WebDirectory       string `yaml:"webDirectory"`
	LandingPage        string `yaml:"landingPage"`
	SchedulesDirectory string `yaml:"schedulesDirectory"`

	Admins              []string            // usernames
	AuthorizedProviders map[string][]string `yaml:"authorizedProviders"` // ["someusername", "127.0.0.1/32"]
}

func loadConfigfile(filename string) ServerConfig {
	log.Println("Reading config file:", filename)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	cfg := ServerConfig{}
	decoder := yaml.NewDecoder(file)
	if err := decoder.Decode(&cfg); err != nil {
		log.Fatal(err)
	}

	return cfg
}
