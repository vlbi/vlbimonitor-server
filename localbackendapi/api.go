package localbackendapi

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"time"
)

type R2dbePlotData struct {
	EightBitStateCounts0    [256]int
	EightBitStateCounts1    [256]int
	TwoBitStateCounts0      [4]int
	TwoBitStateCounts1      [4]int
	EightBitSpectrum0       [2048]float32
	EightBitSpectrum1       [2048]float32
	TwoBitSpectrum0         [2048]float32
	TwoBitSpectrum1         [2048]float32
	BDCAttenuatorParameter0 string
	BDCAttenuatorParameter1 string
	RecorderPrefix0         string
	RecorderPrefix1         string
	RecorderSubgroupValue0  string
	RecorderSubgroupValue1  string
}

type LocalBackendStore struct {
	PlotData         []R2dbePlotData
	UpdateTime       int64 // unix time, compatible with time.Unix()
	LocalObservatory string
	//users    *userapi.UserCache
}

func NewLocalBackendStore(local string) *LocalBackendStore {
	return &LocalBackendStore{
		PlotData:         make([]R2dbePlotData, 4), // TODO; make configurable?
		UpdateTime:       0,
		LocalObservatory: local,
	}
}

func (store *LocalBackendStore) GetHandle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	// auth handled by wrapper in main
	json.NewEncoder(w).Encode(store)
}

func (store *LocalBackendStore) PostHandle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	// writing requires request from localhost

	// unmarshall json body
	body, _ := ioutil.ReadAll(r.Body)

	//data := make(map[string])
	if err := json.Unmarshal(body, &store.PlotData); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	store.UpdateTime = time.Now().Unix()
}
