Design
======
This document describes the overall design of the Radboud Radio Lab VLBI Monitor.

Backstory
---------
The Radboud Radio Lab VLBI monitor was developed for the Event Horizon
Telescope project. This project performs Very Long Baseline Interferometry
measurement campaigns with a worldwide array of radio telescope
observatories operating at millimeter frequencies. The actual data is
collected independently by each observatory and combined offline.
However, during the campaign it is important for the observers at each
observatory to be aware of the status of all observatories in the array.
Additionally the observers need to communicate and log any anomalies that
occur. All this information also needs to be available after the fact when
the measurements are combined, calibrated and analysed.

Before the availability of the VLBI monitor, keeping track of all this
information was done through email, telephone and editing a collective wiki.
While this approach worked, it did not always present a fully consistent
and up-to-date view of the status of all observatories to the observers.
Moreover, the information was unstructured and difficult to retrieve later
by means of automated processes.

Therefore the VLBI monitor was designed to do the following:

1. Present a real-time overview of the status of all observatories in a 
   VLBI campaign to all observers.

2. Enable communication and sharing of log files between observers.

3. Store all metadata, logs and communication for retrieval after the
   measurement campaign has ended.

Requirements
------------
The design of the software was based on the following requirements.

1. It shall be as easy as possible for observatories to opt-in and send
   data to the server. Therefore:
	- the system shall impose an absolute minimum number of
	  requirements / dependencies on the client (sender) side;
	- the interface shall be flexible, enabling scripts / programs
          to be written in any programming language to be either completely
          stand alone (e.g. a Python script grepping from a local log file)
          or fully integrated (e.g. within a Java / C++ based telescope
	  control system);
	- the system shall require only a minimum number of security
	  privileges (i.e. no open ports and no super user access).

2. The system shall be flexible, but this flexibility shall not compromise
   data integrity and accessibility. Therefore:
	- it shall be easy to add metadata fields,
	- but it shall also be easy to impose a required structure and
          ensure conformity.

3. User scripts and the web front end shall be treated equally. Thus:
	- both shall access the metadata through the same open interface.

4. Telescope / instrument metadata and user written logs / messages are
   equally important to track a measurement campaign. Therefore:
	- both shall be gathered in the same data store.

5. Metadata fields may arrive from different sources with different
   frequencies. Therefore:
	- the system will allow for incomplete records to arrive with
	  different frequencies from multiple sources in parallel.

Design decisions
----------------
Based on the requirements the following design decisions were made.

1. All communication with the server shall go through simple HTTP requests.

2. Requests shall be plain text, formatted according to the JSON RPC 2.0
   standard.

3. The standard Auth extension to the JSON RPC 2.0 standard shall be used
   for user and session authentication.

4. Request payloads (e.g. metadata records) shall also be in JSON format.

5. Metadata fields are combined into records. Individual records may be
   incomplete, but are merged into the last record before being stored.
   As such a complete overview of all metadata from a facility at a given
   time is always available with a single read.

6. All metadata records shall be validated against a configurable JSON
   schema, before insertion, to ensure conformity and configurability.

Technology decisions
--------------------
To implement the system the following technologies were chosen.

1. The server shall be developed in the Go programming language. This
   language has wide industry support and was specifically designed to
   ease development of highly concurrent, fast and stable  web server
   backend systems.

2. The database backend shall be MongoDB, but this shall be easily
   switchable at a later stage if benchmarking reveals that this is
   necessary. MongoDB is a natural choice for record (document) based
   storage. It natively uses JSON and a binary (BSON) on disk representation
   providing good compression and easy integration with Go through
   the excellent Mgo package. Moreover, MongoDB is widely used in industry
   and offers automatic replication over multiple servers for safety.

Implementation
--------------
Here some of the implementation details are given. These implementation
details are not authoritative (i.e. if a function behaves different than
described in the documentation it is the documentation that is in error and
needs to be updated). The final reference for server side code
shall always be the source code itself. The final reference for a correctly
formatted request / record shall be the relevant standard (i.e. JSON,
JSON RPC 2.0) and the JSON schema provided by the server.

### Metadata records
Metadata from a facility (observatory) is organized into fields. Each
field consists of:

- a timestamp (in UTC and ISO8601 format);
- a value (of integer, float, boolean, string or coordinate type);
- an optional expiration time (also in UTC and ISO8601 format).

A field is identified by a field name which consists of one or more named
identifiers (levels) separated by a period (`.`). All field names are
given in camelCase, starting with a lower case letter (we follow the Google
style guide for all JSON text).

The separation with periods allows for hierarchical metadata to be
represented.

Fields are combined into records which also include a header with the
following information (entered by the server):

- the time of inserting the record into the database;
- the username that requested the insert;
- the name of the facility.

An example record is given here:

    {
       "header":{
          "time":"2016-01-23T11:34:44.623545Z",
          "facility":"SPT",
          "username":"admin"
       },
       "metadata":{
          "observingMode":{
             "value":"VLBI",
             "time":"2016-01-23T11:34:44.623545Z"
          },
          "oscillatorFrequencyOffset":{
             "value":0.35,
             "time":"2016-01-23T11:34:44.623545Z"
          },
          "maser":{
             "value":false,
             "time":"2016-01-23T11:34:44.623545Z"
          },
          "maser.chamberTemperature":{
             "value":0.916,
             "time":"2016-01-23T11:34:44.623545Z"
          },
          "maser.vacPumpCurrent":{
             "value":0.75,
             "time":"2016-01-23T11:34:44.623545Z"
          }
       }
    }

And the full update request sent to the server over HTTP would look like:

    {
       "jsonrpc":"2.0",
       "id":0,
       "auth":{
          "username":"admin",
          "password":"admin",
          "facility":"SPT"
       },
       "method":"updateRecords",
       "params":{
          "records":[
             {
                "metadata":{
                   "observingMode":{
                      "value":"VLBI",
                      "time":"2016-01-23T11:34:44.623545Z"
                   },
                   "oscillatorFrequencyOffset":{
                      "value":0.35,
                      "time":"2016-01-23T11:34:44.623545Z",
                      "expires":"2016-02-23T12:00:00.0Z"
                   },
                   "maser":{
                      "value":false,
                      "time":"2016-01-23T11:34:44.623545Z"
                   },
                   "maser.chamberTemperature":{
                      "value":0.916,
                      "time":"2016-01-23T11:34:44.623545Z"
                   },
                   "maser.vacPumpCurrent":{
                      "value":0.75,
                      "time":"2016-01-23T11:34:44.623545Z"
                   }
                }
             }
          ]
       }
    }

where the header part of the record was removed because that is generated
by the server. Note that multiple records can be sent (or returned) in a
single request.

License
-------

Copyright (C) Pim Schellart 2016

- The server is available under the terms of the [GNU Affero General Public License](http://www.gnu.org/licenses/agpl-3.0.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

- All (example) clients are available under the terms of the [MIT License](http://opensource.org/licenses/MIT).

These licenses were chosen for the following reasons:

1. Both are standard open source licenses, widely used in the community.
2. The GNU Affero GPLv3 license is [recommended](http://www.gnu.org/licenses/why-affero-gpl.html) by the Free Software Foundation for web-server like software.
3. The MIT license for (example) clients allows these to be integrated into all open source and closed source programs at the observatories, without requiring them to release their source code or change their license.
