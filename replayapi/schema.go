package replayapi

const schemaCreate = `{
	"properties": {
		"Reason": {"type": "string"},
		"OriginStart": {"type": "integer"},
		"OriginStop": {"type": "integer"},
		"OriginDuration": {
			"type": "integer",
			"minimum": 0,
			"maximum": 864000
		},
		"ServerURL": {"type": "string"},
		"Start": {"type": "integer"},
		"Speed": {"type": "number"},
		"Observatories": {
			"type": "array",
			"items": {"type": "string"},
			"uniqueItems": true
		},
		"KeepOriginalDataTimestamps": {"type": "boolean"},
		"Verbose": {"type": "boolean"}
	},
	"required": ["Reason", "OriginStart", "ServerURL"],
	"oneOf": [{
			"required": ["OriginStop"],
			"not": {"required": ["OriginDuration"]}
		},{
			"required": ["OriginDuration"],
			"not": {"required": ["OriginStop"]}
		}
	],
	"additionalProperties": false
}`
