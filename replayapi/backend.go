package replayapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/userapi"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"github.com/xeipuuv/gojsonschema"
	"log"
	"strings"
	"sync"
	"time"
)

// Job id length
const tokenLength = 32

// generator
func NewReplayStore(userCache *userapi.UserCache, replayer func(Job)) *ReplayStore {
	// load schema
	loader := gojsonschema.NewStringLoader(schemaCreate)
	schema, err := gojsonschema.NewSchema(loader)
	if err != nil {
		log.Panic(err)
	}

	rs := ReplayStore{
		jobs:     make(map[string]Job, 0),
		replayer: replayer,
		schema:   schema,
		users:    userCache,
	}
	return &rs
}

// receiver type
type ReplayStore struct {
	jobs     map[string]Job
	replayer func(Job)
	lock     sync.RWMutex
	schema   *gojsonschema.Schema
	users    *userapi.UserCache
}
type Job struct {
	Id             string
	OriginStart    int64
	OriginStop     int64
	OriginDuration int64
	Observatories  []string `json:,omitempty` // [default: empty list means 'all']
	Start          int64    // Start time [default: NOW]
	Speed          int64    // OriginDuration / Duration
	Reason         string   // Job's purpose
	ServerURL      string
	// report each replay record to be submitted
	Verbose bool
	// data timestamps are time-shifted to now by default
	KeepOriginalDataTimestamps bool
	// non-input fields
	Cancel chan struct{} `json:"-"`
	Done   chan struct{} `json:"-"`
}

func (rs *ReplayStore) insert(job *Job) error {
	// verify input
	msg := make([]string, 0)
	if job.OriginStop > 0 && job.OriginStop <= job.OriginStart {
		msg = append(msg, "OriginStop must be greater than OriginStart")
	}
	if len(msg) != 0 {
		return fmt.Errorf("%s", strings.Join(msg, "; "))
	}

	// parse input
	if job.OriginDuration > 0 {
		job.OriginStop = job.OriginStart + job.OriginDuration
	} else {
		job.OriginDuration = job.OriginStop - job.OriginStart
	}

	// Generate a new random job id
	bytes := make([]byte, tokenLength)
	if _, err := rand.Read(bytes); err != nil {
		return fmt.Errorf("Job ID generation error")
	}
	jobid := base64.URLEncoding.EncodeToString(bytes)

	// insert job
	rs.lock.Lock()
	job.Id = jobid
	rs.jobs[job.Id] = *job
	rs.lock.Unlock()

	// start job
	tuntil := job.Start - time.Now().Unix()
	duration := (job.OriginStop - job.OriginStart) / job.Speed
	log.Printf("replay scheduled: id=%s, untilstart=%ds, duration=%ds, reason='%s'", job.Id, tuntil, duration, job.Reason)
	go rs.replayer(*job)

	return nil
}
