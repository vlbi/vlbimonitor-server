package replayapi

import (
	"bitbucket.org/vlbi/vlbimonitor-server/apisupport"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"time"
)

// receiver type
type Api struct {
	*ReplayStore
}

// implement the Cruder interface
func (rc *Api) Attached(method string) bool {
	switch method {
	case "create":
		return true
	case "list":
		return true
	case "read":
		return true
	case "update":
		return false
	case "replace":
		return false
	case "delete":
		return true
	}
	return false
}

func (rs *Api) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// set defaults
	job := Job{
		Start:  time.Now().Unix(),
		Speed:  1,
		Cancel: make(chan struct{}),
		Done:   make(chan struct{}),
	}

	// decode input
	if err := apisupport.ParseJsonBody(r.Body, rs.schema, &job); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// insert job
	if err := rs.insert(&job); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(job.Id)
}

func (rs *Api) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	rs.lock.RLock()
	json.NewEncoder(w).Encode(rs.jobs)
	rs.lock.RUnlock()
}

func (rs *Api) Read(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	rs.lock.RLock()
	job, ok := rs.jobs[id]
	rs.lock.RUnlock()
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	json.NewEncoder(w).Encode(job)
}

func (rs *Api) Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (rs *Api) Replace(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {}

func (rs *Api) Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	rs.lock.RLock()
	job, ok := rs.jobs[id]
	rs.lock.RUnlock()
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	close(job.Cancel)
	<-job.Done

	// remove job
	rs.lock.Lock()
	delete(rs.jobs, id)
	rs.lock.Unlock()

	log.Println("Delete replay:", id)
}
