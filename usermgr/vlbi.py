# An example client for the Radboud Radio Lab VLBI Monitor
# 
# Copyright (c) 2016 Pim Schellart
# 
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.

"""This module provides client side tools to interface with the VLBI monitor.
"""

import requests
import json
import datetime
import os

class ServerError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

"""BackupWriter is responsible for writing backups of requests to disk.

May be reimplemented to write to some other storage type instead.
"""
class BackupWriter(object):
    def __init__(self, outdir):
        self.outdir = outdir

    def write(self, request):
        filename = "{0}-{1}-{2}-{3}.json".format(request["method"],
                request["auth"]["username"],
                request["auth"]["facility"],
                datetime.datetime.utcnow().isoformat())

        with open(os.path.join(self.outdir, filename), "w") as f:
            json.dump(request, f)

class Client(object):
    headers = {'content-type': 'application/json'}

    def __init__(self, servers, facility, username, password):
        assert isinstance(servers, list)
        self.servers = servers
        self.facility = facility
        self.username = username
        self.password = password

    def postone(self, request):
        response = requests.post(self.servers[0], data=json.dumps(request), headers=self.headers).json()
    
        if "error" in response:
            raise ServerError(str(response["error"]["message"]))

        if "results" in response:
            return response["results"]

    def post(self, request):
        #-- submit to servers
        responses = []
        for server in self.servers:
            response = requests.post(server, data=json.dumps(request), headers=self.headers).json()
            responses.append(response)
    
            if "error" in response:
                print server, response["error"]["message"]

        return responses

    @property
    def auth(self):
        return {"facility": self.facility, "username": self.username, "password": self.password}

    """List all valid methods.
    """
    def listMethods(self):
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "listMethods", "id": 0}
    
        return self.postone(request)

    """Send new data record to servers. At the servers this new data record is merged with current record and stored.
    """
    def submitRecord(self, record):
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "submitRecord", "params": {"record": record}, "id": 0}
    
        return self.post(request)
    
    """Get the current record for the specified list of *observatories* and *fields*.
    """
    def getSnapshots(self, observatories=[], fields=[]):
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getSnapshots", "params": {"observatories": observatories, "fields": fields}, "id": 1}

        return self.postone(request)
    
    """Get a single *field* as a function of time between *startTime* and *endTime*.

    If a *startTime* and/or *endTime* is/are provided the search is restricted to this range. Both parameters need to be of the standard library ``datetime.datetime'' type. If either *startTime* or *endTime* is missing the range will be infinite in the respective direction.

    The output will be a time ordered list of field dictionaries containing ``time'' and ``value'' attributes.
    """
    def getParamHistory(self, observatory, field, startTime, endTime):
        if startTime and endTime:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getSingleFieldTimeseries", "params": {"observatories": [observatory], "fields": [field], "startTime": startTime.isoformat('T') + 'Z', "endTime": endTime.isoformat('T') + 'Z'}, "id": 0}
        elif startTime:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getSingleFieldTimeseries", "params": {"observatories": [observatory], "fields": [field], "startTime": startTime.isoformat('T') + 'Z'}, "id": 0}
        elif endTime:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getSingleFieldTimeseries", "params": {"observatories": [observatory], "fields": [field], "endTime": endTime.isoformat('T') + 'Z'}, "id": 0}
        else:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getSingleFieldTimeseries", "params": {"observatories": [observatory], "fields": [field]}, "id": 0}
    
        return self.postone(request)

    """Send a string *message*.
    """
    def addMessage(self, message):
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "addMessages", "params": {"messages": [message]}, "id": 0}
    
        return self.post(request)

    """Send a log *message* string.
    """
    def addLog(self, message):
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "addLogs", "params": {"messages": [message]}, "id": 0}
    
        return self.post(request)
    
    """Get a list of message strings from one of the servers.

    If a *startTime* and/or *endTime* is/are provided the search is restricted to this range. Both parameters need to be of the standard library ``datetime.datetime'' type. If either *startTime* or *endTime* is missing the range will be infinite in the respective direction.
    """
    def getMessages(self, startTime, endTime):
        if startTime and endTime:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getMessages", "params": {"startTime": startTime.isoformat('T') + 'Z', "endTime": endTime.isoformat('T') + 'Z'}, "id": 0}
        elif startTime:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getMessages", "params": {"startTime": startTime.isoformat('T') + 'Z'}, "id": 0}
        elif endTime:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getMessages", "params": {"endTime": endTime.isoformat('T') + 'Z'}, "id": 0}
        else:
            request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getMessages", "id": 0}
    
        return self.postone(request)
    
    """Change user password.
    """
    def changePassword(self, username, password):
        user = {"username" : username, "password": password}
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "changePassword", "params": {"user": user}, "id": 0}
    
        return self.post(request)

    """Add user.

    Note that only admins can do this.
    """
    def addUser(self, username, password, name=None, email=None, permissions=None):
        user = {"username": username, "password": password,
                "name": name, "email": email, "permissions": permissions}

        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "addUser", "params": {"user": user}, "id": 0}

        return self.post(request)

    """Remove user.

    Note that only admins can do this.
    """
    def removeUser(self, username):
        user = {"username" : username}
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "removeUser", "params": {"user": user}, "id": 0}
    
        return self.post(request)

    """Get user info.

    Note that only admins can do this.
    """
    def getUser(self, username):
        user = {"username" : username}
        request = {"jsonrpc": "2.0", "auth": self.auth, "method": "getUser", "params": {"user": user}, "id": 0}
    
        return self.postone(request)

