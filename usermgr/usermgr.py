#!/usr/bin/python2
# An example client for the Radboud Radio Lab VLBI Monitor
#
# Copyright (c) 2016 Pim Schellart
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.

import vlbi
import getpass
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--server_admin", default="admin", help="Username of server admin")
subparsers = parser.add_subparsers(dest='action')

subparser = subparsers.add_parser('add', help='add user')
subparser.add_argument("username")
subparser.add_argument("-f", "--facility", nargs="+", required=True, help="One or more facilities")
subparser.add_argument("-m", "--methods", nargs="+", help="One or more methods")
subparser.add_argument("--name", help="Real name", default="")
subparser.add_argument("--email", help="Email address", default="")

subparser = subparsers.add_parser('add-multi', help='add users from username/password file')
subparser.add_argument("fname", help="Filename of text file containing Username/Password combinations")
subparser.add_argument("-m", "--methods", nargs="+", help="One or more methods")
subparser = subparsers.add_parser('remove-multi', help='add users from username/password file')
subparser.add_argument("fname", help="Filename of text file containing Usernames")

subparser = subparsers.add_parser('remove', help='remove user')
subparser.add_argument("username")

subparser = subparsers.add_parser('get-info', help='print user permissions')
subparser.add_argument("username")

subparser = subparsers.add_parser('change-password', help='change password')
subparser.add_argument("username")

args = parser.parse_args()

admin_facility = "RU"
#servers = ["http://casdev.science.ru.nl/rpc"]
servers = ["http://localhost:8000/rpc"]
#servers = ["https://vlbimon1.science.ru.nl/rpc",
#           "https://vlbimon2.science.ru.nl/rpc"]


if args.action == 'add':
    server_password = getpass.getpass("Password for {0}:".format(args.server_admin))

    client = vlbi.Client(servers, admin_facility, args.server_admin, server_password)

    allmethods = client.listMethods()
    #-- complain if no method selected
    if args.methods is None: raise Exception("No method selected: {:}".format(','.join(allmethods)))
    #-- expand the "all" wildcard
    if len(args.methods) == 1  and  args.methods[0] == "all": args.methods = [m for m in allmethods]
    #-- allow the * wildcard
    allmethods.append("*")

    permissions = {}
    for method in args.methods:
        if not method in allmethods: raise Exception("method {:} unknown. Known methods: {:}".format(method, ','.join(allmethods)))
        permissions[method] = {}
        for facility in args.facility:
            permissions[method][facility] = True
    permissions["changePassword"] = {"*": True}

    password = getpass.getpass("New user password:")
    try:
        client.addUser(args.username, password, name=args.name, email=args.email, permissions=permissions)
    except vlbi.ServerError as e:
        print "Error: {0}".format(str(e))


elif args.action == 'add-multi':
    server_password = getpass.getpass("Password for {0}:".format(args.server_admin))
    
    client = vlbi.Client(servers, admin_facility, args.server_admin, server_password)

    allmethods = client.listMethods()
    #-- complain if no method selected
    if args.methods is None: raise Exception("No method selected: {:}".format(','.join(allmethods)))
    #-- expand the "all" wildcard
    if len(args.methods) == 1  and  args.methods[0] == "all": args.methods = [m for m in allmethods]
    #-- allow the * wildcard
    allmethods.append("*")

    with open(args.fname, 'r') as f:
        upf = f.readlines()

    for line in upf:
        u, p, f = line.strip().split('\t')

        permissions = {}
        for method in args.methods:
            if not method in allmethods: raise Exception("method {:} unknown. Known methods: {:}".format(method, ','.join(allmethods)))
            permissions[method] = {}
            permissions[method][f] = True
        permissions["changePassword"] = {"*": True}

        print "{:}\t{:}\t{:}".format(u, p, f)
        try:
            client.addUser(u, p, permissions=permissions)
        except vlbi.ServerError as e:
            print "Error: {0}".format(str(e))


elif args.action == 'remove-multi':
    server_password = getpass.getpass("Password for {0}:".format(args.server_admin))

    client = vlbi.Client(servers, admin_facility, args.server_admin, server_password)

    with open(args.fname, 'r') as f:
        upf = f.readlines()

    for line in upf:
        u = line.strip().split('\t')[0]

        print u
        try:
            client.removeUser(u)
        except vlbi.ServerError as e:
            print e


elif args.action == 'remove':
    server_password = getpass.getpass("Password for {0}:".format(args.server_admin))

    client = vlbi.Client(servers, admin_facility, args.server_admin, server_password)

    try:
        client.removeUser(args.username)
    except vlbi.ServerError as e:
        print e

elif args.action == 'get-info':
    server_password = getpass.getpass("Password for {0}:".format(args.server_admin))

    client = vlbi.Client(servers, admin_facility, args.server_admin, server_password)

    try:
        info = client.getUser(args.username)

        print "Username:", info["username"]
        print "Permissions:", info["permissions"]
        print "Is admin:", info["isAdmin"]

    except vlbi.ServerError as e:
        print e

elif args.action == 'change-password':
    old_password = getpass.getpass("Password for {0}:".format(args.username))
    new_password = getpass.getpass("New password:")

    client = vlbi.Client(servers, admin_facility, args.username, old_password)

    try:
        client.changePassword(args.username, new_password)
    except vlbi.ServerError as e:
        print "Error: {0}".format(str(e))

else:
    print "action {:} not implemented".format(args.action)
