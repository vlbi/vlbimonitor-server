package restful

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type Cruder interface {
	Attached(string) bool
	Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params)
	List(w http.ResponseWriter, r *http.Request, _ httprouter.Params)
	Read(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Replace(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
	Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
}

type Wrapper func(httprouter.Handle) httprouter.Handle

// Attach a Cruder implementation to a set of endpoints
func Attach(router *httprouter.Router, endpoint string, crud Cruder, f Wrapper) {
	if crud.Attached("create") {
		router.POST(endpoint, f(crud.Create))
	}
	if crud.Attached("list") {
		router.GET(endpoint, f(crud.List))
	}
	if crud.Attached("read") {
		router.GET(endpoint+"/:id", f(crud.Read))
	}
	if crud.Attached("update") {
		router.PATCH(endpoint+"/:id", f(crud.Update))
	}
	if crud.Attached("replace") {
		router.PUT(endpoint+"/:id", f(crud.Replace))
	}
	if crud.Attached("delete") {
		router.DELETE(endpoint+"/:id", f(crud.Delete))
	}
}
