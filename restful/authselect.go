package restful

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

// authentication selector
func AuthOr(ff ...func(r *http.Request) bool) Wrapper {
	return func(h httprouter.Handle) httprouter.Handle {
		return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			for _, f := range ff {
				if f(r) {
					goto auth_ok
				}
			}
			// Request Basic Authentication
			//w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return

		auth_ok:
			h(w, r, ps)
		}
	}
}

// authentication selector
func AuthAnd(ff ...func(r *http.Request) bool) Wrapper {
	return func(h httprouter.Handle) httprouter.Handle {
		return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			for _, f := range ff {
				if !f(r) {
					goto auth_bad
				}
			}
			// Delegate request to the given handle
			h(w, r, ps)
			return

		auth_bad:

			// Request Basic Authentication otherwise
			//w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		}
	}
}
