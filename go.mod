module bitbucket.org/vlbi/vlbimonitor-server

go 1.17

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.0
	github.com/pkg/errors v0.9.1
	github.com/xeipuuv/gojsonschema v1.2.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
