#!/usr/bin/env python
import requests
#from pprint import pprint
import json
import yaml
import calendar
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.dates as md
import os
import glob
import argparse
import re
import time

def date_utime(s):
    from dateutil import parser
    import calendar
    if len(s) == 10:
        t0 = "{:4s}-{:2s}-{:2s}T{:2s}:00:00Z".format(s[0:4], s[4:6], s[6:8], s[8:10])
    elif len(s) == 12: 
        t0 = "{:4s}-{:2s}-{:2s}T{:2s}:{:2s}:00Z".format(s[0:4], s[4:6], s[6:8], s[8:10], s[10:12])
    else:
        raise Exception('date string supported has len 10 or 12')
    return calendar.timegm(parser.parse(t0).timetuple())

def fname_utime(fname):
    m = re.search('[0-9]{10}', fname)
    return date_utime(m.group())

class MarsFile():
    """ECMWF Mars data file class"""
    def __init__(self, fname):
        self.read(fname)

    def read(self, fname):
        print("reading:", fname)
        obs, self.data, block = '', {}, []
        with open(fname, 'r') as f:
            #-- file header
            f.readline()
            while True:
                line = f.readline().strip()
                words = line.split()

                #-- block header or EOF
                if line == ''  or  line[0] == '#':
                    #-- bogus line
                    if len(words) == 1: continue

                    #-- insert data
                    if len(block) > 0:
                        self.data[obs] = np.array(block).transpose()

                    #-- EOF
                    if line == '': break

                    #-- init next block
                    obs = words[2]
                    block = []
                    continue

                #-- eof
                if line == '': break

                #-- corrupt data
                if len(words) != 14: continue

                #-- block body
                #-- time
                hour = int(int(words[3])/100 + int(words[4]))
                dt = np.timedelta64(hour, 'h')
                day = words[2]
                t = np.datetime64('{:}-{:}-{:}'.format(day[:4], day[4:6], day[6:8]), 's')
                t = (t + dt).astype(int)

                block.append([t] + [np.array([float(words[i]) for i in (10,11,12,13)]).mean()])

        #-- rename
        self.data['GLT'] = self.data.pop('Thule')
        self.data['PICO'] = self.data.pop('IRAM_PV')
        self.data['NOEMA'] = self.data.pop('IRAM_PdB')
        self.data['SPT'] = self.data.pop('SPTDUMMY')

        self.x = self.data[obs][0]


#-- setup parser
parser = argparse.ArgumentParser(description='')
parser.add_argument("-f", "--config", required=True, help='Configuration file')
parser.add_argument("--server", default='http://localhost:8000', help='username for server authentication')
parser.add_argument("--username", default='admin', help='username for server authentication')
#-- parse arguments
args = parser.parse_args()


#-- config file
with open(args.config, "r") as f:
    conf = yaml.load(f, Loader=yaml.Loader)
    #-- sanity check
    for p in ["model_toffset", "model_linecolor", "xlim"]:
        assert isinstance(conf["tauPlots"][p], list)
        assert len(conf["tauPlots"][p]) > 0
    for p in ["width", "height", "dpi", "width2", "height2", "dpi2"]:
        assert isinstance(conf["tauPlots"][p], int)

#-- fudge factor tau calibration
def TAUFUDGE(obs):
    if obs in ["LMT", "SMA"]: return .33
    if obs in ["ALMA", "APEX", "PICO"]: return .85
    if obs in ["JCMT"]: return .4
    if obs in ["SMTO"]: return .66
    if obs in ["GLT"]: return 1.8
    if obs in ["KP"]: return .5
    if obs in ["NOEMA"]: return .45
    return 1

width = conf["tauPlots"]["width"]/float(conf["tauPlots"]["dpi"])
height = conf["tauPlots"]["height"]/float(conf["tauPlots"]["dpi"])
dpi = conf["tauPlots"]["dpi"]
width2 = conf["tauPlots"]["width2"]/float(conf["tauPlots"]["dpi2"])
height2 = conf["tauPlots"]["height2"]/float(conf["tauPlots"]["dpi2"])
dpi2 = conf["tauPlots"]["dpi2"]

#-- target model dates and line colors
model_toffset = conf["tauPlots"]["model_toffset"]
model_linecolor = conf["tauPlots"]["model_linecolor"]
assert len(model_toffset) <= len(model_linecolor)

#-- time window
tnow = int(time.time())
dtime = max(model_toffset)
tstart = tnow - dtime*86400
tend = tnow + dtime*86400

########################################################################
#-- model files
########################################################################
path = os.path.expandvars(conf["weatherModelDirectory"])
fnames = sorted(glob.iglob(path + '/BHC_20????????.dat'))

TARGET_HISTORY_DAYS = 10
DATASETS_PER_DAY = 2
DATA_LENGTH_DAYS = 10
DATA_POINTS_PER_DAY = 24

#-- last n days
dates = np.array([fname_utime(fname) for fname in fnames])
first = np.searchsorted(dates, tnow - (dtime + DATA_LENGTH_DAYS)*86400)  #-- +10 to fill up the history
if first > 0: first -= 1
fnames = fnames[first:]

#-- first and last time points
date0 = fnames[0].lstrip(path + '/BHC_').rstrip('.dat')
t0 = date_utime(date0)
date1 = fnames[-1].lstrip(path + '/BHC_').rstrip('.dat')
t1 = date_utime(date1) + (DATA_LENGTH_DAYS * DATA_POINTS_PER_DAY) * 60**2
#-- number of hours
nt = int((t1 - t0) / 60**2 + 1)
#-- number of time slices
ns = DATA_LENGTH_DAYS * DATASETS_PER_DAY + 1
print("number of hours %d, number of time slices %d" % (nt, ns))

#-- time coordinates
xmod = np.arange(nt) * 60**2 + t0

models = []
sites = set()
for fname in fnames:
    mf = MarsFile(fname)
    d = mf.data
    models.append(d)
    sites.update(d.keys())
sites = list(sites)

#-- value coordinates
#-- create a matrix for each site
ymod = {s: np.zeros((ns, nt)) for s in sites}
for mod in models:
    for obs, arr in mod.items():
        x = arr[0]
        indx = ( (x - t0) / 60**2).astype('int')
        indy = ( (x - x[0]) / 60**2 / 12).astype('int')
        ymod[obs][indy, indx] = arr[1]

##-- save result to file
#for k,v in ymod.items():
#    outarr = np.vstack((xmod, v))
#
#    #-- drop first 10 days, they are not consecutive
#    offset = DATA_LENGTH_DAYS * 24
#    outarr = np.vstack((xmod[offset:], v[:, offset:]))
#
#    fname = 'model-'+k+'.txt'
#    print('saving:', fname)
#    np.savetxt(fname, outarr.transpose())#, fmt='%10i %.3f')

#-- collapse into 'most-recent' vector
idxmask = np.zeros((nt))
yflat = {s: np.zeros((nt)) for s in sites}
for k, v in ymod.items():
    for i in reversed(range(ns)):
        idx = np.nonzero(v[i])
        idxmask[idx] = 1
        yflat[k][idx] = v[i][idx]
idx = np.nonzero(idxmask)
xflat = xmod[idx]

##-- save vectors to file using pandas
#df = pd.DataFrame(yflat, index=xflat)
#fname = 'yflat.txt'
#print('writing:', fname)
#df.to_csv(fname)

#-- local storage directory
storagedir = os.path.join(conf["webDirectory"],"img/plots/")
# this expand (among other things) ${HOME}
storagedir = os.path.expandvars(storagedir)

#-- create storage dir if not exists
try:
    os.mkdir(storagedir)
    print("creating directory: " + storagedir)
except OSError:
    pass

labels = list(yflat.keys())
table = np.zeros((len(yflat)+1, len(xflat)))
table[0] = xflat
for i, k in enumerate(yflat.keys()):
    table[i+1, :] = TAUFUDGE(k) * yflat[k][idx]/19
fname = storagedir + 'tau225.txt'
print('writing:', fname)
with open(fname, 'w') as f:
    f.write(('{:12s}'*(len(yflat)+1) + '\n').format('time', *list(yflat.keys())))
    np.savetxt(f, table.transpose(), fmt="%#.10g")

########################################################################


#-- fetch tau data
datafield = "weather_tau225"
obs = "&observatory=".join(conf["observatories"])
urlquery = "/data/history?observatory={:}&field={:}&startTime={:}&endTime={:}".format(obs, datafield, tstart, tend)

args.server.replace('localhost:8000', 'localhost%s'%conf["serverAddress"])
print("server:", args.server)

session = requests.Session()
#session.auth = ('username', 'password')
r = session.get(args.server + urlquery)
#pprint(r.json())

#-- process tau data
odata = {}
osource = {}
otodo = []
for obs, obsdata in r.json().items():
    data = np.array(obsdata[datafield], dtype='float64')
    if data.shape[0] <= 1:
        otodo.append(obs)
        continue
    data = data.transpose()
    odata[obs] = data[:]
    osource[obs] = "tau225"

#-- missing data
for obs in conf["observatories"]:
    if obs in odata:
        continue
    otodo.append(obs)

#-- fetch pwv data if tau not available
datafield = "weather_pwv"
obs = "&observatory=".join(otodo)
urlquery = "/data/history?observatory={:}&field={:}&startTime={:}&endTime={:}".format(obs, datafield, tstart, tend)
r = session.get(args.server + urlquery)
#pprint(r.json())

#-- process pwv data
for obs, obsdata in r.json().items():
    data = np.array(obsdata[datafield], dtype='float64')
    if data.shape[0] <= 1:
        #-- fake two data points to enable gap detection
        data = np.array([[tstart, tnow],[0, 0]])
        osource[obs] = "none"
    else:
        data = data.transpose()
        data[1] *= .058
        data[1] += .004
        osource[obs] = "pwv"
    odata[obs] = data[:]

#-- still missing data
for obs in conf["observatories"]:
    if obs in odata:
        continue
    osource[obs] = "none"
    odata[obs] = np.array([[tstart, tnow],[0, 0]])

#-- sort data
for obs in odata:
    data = odata[obs]
    x = data[0]
    idx = np.argsort(x)
    odata[obs] = data[:, idx]

#-- fix negatives (this happens at LMT for some reason)
for obs, data in odata.items():
    data[1] = np.abs(data[1])

#-- fix beginning and end points
for obs in odata:
    #-- extend
    d = np.ma.array(np.zeros([2, len(odata[obs][0])+2]))
    d[:, 0] = [tstart, np.ma.masked]
    d[:, -1] = [tnow, np.ma.masked]
    d[:, 1:-1] = odata[obs]
    #-- drop extension if not needed
    if d[0, 1] < tstart:
        d = d[:, 1:]
    if d[0, -2] > tend:
        d = d[:, :-1]
    odata[obs] = d

#-- detect and mask gaps
odata_gaps = {}
for obs in odata:
    t = odata[obs][0]
    dt = np.diff(t)
    #-- large gap
    idx = np.nonzero(dt > 7500)[0]
    if len(dt) == 1:
        idx = np.array([0])
    #-- save gap boundaries
    odata_gaps[obs] = np.vstack((t[idx], t[idx + 1])).transpose()
    #-- apply mask
    odata[obs][1, idx] = np.ma.masked

ymax = .7

#-- generate plots
for obs, data in odata.items():
    print("processing {}: {}".format(osource[obs], obs))

    fig = plt.figure()

    ax = fig.add_subplot(111)

    #-- plot gaps
    for tup in odata_gaps[obs]:
        ax.axvspan(*np.array(tup, dtype='datetime64[s]'), color="#ffcccc", zorder=0)

    #-- plot model
    if not obs in ymod:
        print("model missing:", obs)
        continue

    y = TAUFUDGE(obs) * yflat[obs]/19
    idx = np.nonzero(y)
    y  = y[idx]
    x = np.array(xmod[idx], dtype='datetime64[s]')
    ax.plot(x, y, linewidth=2, color='k')

    ax.set_xmargin(0)

    #ax.spines['top'].set_visible(False)
    #ax.spines['right'].set_visible(False)

    ax.tick_params(axis='y', which="major", direction="in",
        left=True, reset=False, labelleft=True, pad=2)

    x = np.array(data[0], dtype='datetime64[s]')
    ax.fill_between(x, 0, data[1], linewidth=0)
    ax.set_axisbelow(True)
    ax.xaxis_date(tz='UTC')

    ax.grid(color=".8")

    ax.set_ylim([0, ymax])

    ax.axvline(np.datetime64(int(tnow), 's'), linewidth=1, color="k")


    for dtime in conf["tauPlots"]["xlim"]:
        locator = md.AutoDateLocator(minticks=6, maxticks=11)
        ax.xaxis.set_major_locator(locator)
        if dtime > 2:
            ax.xaxis.set_major_formatter(md.DateFormatter('%d'))
        else:
            ax.xaxis.set_major_formatter(md.DateFormatter('%d-%Hh'))

        tmin = tnow - dtime*86400
        tmax = tnow + dtime*86400
        ax.set_xlim(np.array([tmin, tmax], dtype='datetime64[s]'))

        text = []
        x = np.datetime64(int(tnow - .01*(tmax - tmin)), 's')
        t = ax.text(x, .85*ymax, "PAST", color='k', ha='right')
        text.append(t)
        t = ax.text(x, .85*ymax, "FUTURE", color='k', ha='left')
        text.append(t)
        x = np.datetime64(int(tmin + .95*(tmax - tmin)), 's')
        t = ax.text(x, .85*ymax, "Tau225", color='k', ha='right', bbox={'ec': 'k', 'fc': 'w'})
        text.append(t)

        y = .85*ymax
        xleft = tmin + .01*(tmax-tmin)
        xright = tnow - .14*(tmax-tmin)

        #ax.legend(labelspacing=0, columnspacing=0, handlelength=.75, framealpha=.6, loc='center right')

################
        #x = np.datetime64(m.x[0])
        #x = max(xleft, x)
        #if x > xright: y = min(y, .7*ymax)
        #t = ax.text(x, y, m.label, color=m.color, ha='left')
        #if x > xright: y = .85*ymax #-- reset
        #if x == xleft: y -= .15*ymax
        #text.append(t)
################

        imgfile = os.path.join(storagedir, "tau225.{:}.d{:d}.png".format(obs, dtime))
        print("saving:", imgfile)
        ax.set_position((0.06, 0.15, 1-0.06, 0.82))
        fig.set_size_inches(width, height)
        fig.savefig(imgfile, pad_inches=0, dpi=dpi)

        imgfile = os.path.join(storagedir, "tau225.{:}.d{:d}.wide.png".format(obs, dtime))
        print("saving:", imgfile)
        print(ax.get_position())
        ax.set_position((0.025, 0.12, 1-0.04, 0.85))
        fig.set_size_inches(width2, height2)
        fig.savefig(imgfile, pad_inches=0, dpi=dpi2)


        [t.remove() for t in text]
        del text

    plt.close(fig)
