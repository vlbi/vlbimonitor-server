#!/usr/bin/python
from __future__ import print_function
import requests
import datetime

server = 'vlbimon1'
SERVER = 'https://%s.science.ru.nl' % server
FNAME = '.sessionid.%s.txt' % server

def get_session():
    '''Setup user session with server'''
    def restore_session(s):
        #-- read from file
        with open(FNAME, 'r') as f:
            sessionid = f.readline().strip()
        #-- validate
        s.cookies.set('sessionid', sessionid)
        url = SERVER + '/session/' + sessionid
        r = s.patch(url)
        r.raise_for_status()
        return s
        
    def create_session(s):
        url = SERVER + '/session'
        import getpass
        username = input("Username:")
        password = getpass.getpass("Password for {:}:".format(username))
        r = s.post(url, auth=requests.auth.HTTPBasicAuth(username, password))
        r.raise_for_status()
        resp = r.json()
        sessionid = resp['id']
        s.cookies.set('sessionid', sessionid)
        #-- write to file
        with open(FNAME, 'w') as f:
            f.write(sessionid)
        return s

    s = requests.Session()
    #-- restore session
    try:
        return restore_session(s)
    except: pass
    #-- create new session if restore failed
    return create_session(s)

def fetch_timeseries(s, timebracket, sites=[], fields=[]):
    '''Fetch metadata timeseries data from server'''
    url = SERVER + '/data/history'

    tail = ['observatory=' + s for s in sites]
    tail += ['field=' + f for f in fields]
    tail += ['startTime=%d' % timebracket[0], 'endTime=%d' % timebracket[1]]
    if len(tail) > 0:
        url += '?' + '&'.join(tail)

    r = s.get(url)
    r.raise_for_status()
    return r.json()

def report_remaining_disk_space(s):
    sites = ['ALMA','APEX','GLT','JCMT','KP','LMT','NOEMA','PICO','SMA','SMTO','SPT']
    fields = ['recorder_{}_group{}SpaceLeft'.format(r,g) for r in [1,2,3,4] for g in 'abcd']
    timebracket = [t.timestamp() for t in [datetime.datetime(2022,1,1), datetime.datetime.utcnow()]]
    result = fetch_timeseries(s, timebracket, sites, fields)

    for site,d in list(result.items()):
        print(site)
        if site == 'SMA':
            rate_Gbps = 2*8.504*4576.0/4096.0
        else:
            rate_Gbps = 2*8.224
        for r in [1,2,3,4]:
            space_GB = 0
            n_KeyOrIndexError = 0
            last_updates = set()
            for g in 'abcd':
                field = 'recorder_{}_group{}SpaceLeft'.format(r,g)
                try:
                    timeseries = d[field]
                    space_GB += timeseries[-1][1]
                    last_updates.add(timeseries[-1][0])
                except (KeyError,IndexError): n_KeyOrIndexError += 1
            if n_KeyOrIndexError == 4:
                print("    (no data for recorder{})".format(r))
                continue
            space_h = space_GB*8/rate_Gbps/3600
            last_update = datetime.datetime.fromtimestamp(sorted(last_updates)[-1])
            update_str = "{}{}".format(last_update.strftime("%Y-%m-%d %H:%M:%S")," (some subgroups older)" if len(last_updates) > 1 else "")
            print("    recorder{} has {:7.0f} GB = {:6.3f} h @ {:6.3f} Gbps remaining [{}]".format(r, space_GB, space_h, rate_Gbps, update_str))

    return result

def main():
    session = get_session()

    report_remaining_disk_space(session)

if __name__ == '__main__':
    main()

# vim: foldmethod=indent
