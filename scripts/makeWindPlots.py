#!/usr/bin/env python2
import getpass
import requests
# import plotconf as conf #-- vlbimon configuration file
from pprint import pprint
import json
import yaml
import shutil
import datetime
import calendar
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.dates as md
import matplotlib.ticker as ticker
import os
import glob
import argparse

class MarsFile():
    """ECMWF Mars data file class"""
    def __init__(self, fname):
        self.read(fname)

    def read(self, fname):
        print "reading:", fname
        data = np.loadtxt(fname, usecols=(2,3,4,5,9,10,11,12,13)).transpose()

        obs = ["DeBilt", "LMT", "SMA", "NOEMA", "PICO", "SPT", "SMTO", "JCMT", "ALMA", "APEX", "GLT", "Thule", "AMT", "KP"]

        day = data[0]
        hour = data[1]/100 + data[2]

        def utime(day, hour):
            d = "{:08d}".format(int(day))
            t = float(datetime.datetime.strptime(d, "%Y%m%d").strftime("%s"))
            dt = datetime.timedelta(hours=hour)
            return t + dt.total_seconds()
        utimev = np.vectorize(utime)

        nobs = np.count_nonzero(data[2] == 0.)
        x = utimev(day, hour).reshape([nobs, -1])
        y = np.mean(data[5:], axis=0).reshape([nobs, -1])

        self.data = {}
        nobs = min(nobs, len(obs))
        for i in range(nobs):
            o = obs[i]
            self.data[o] = np.vstack((x[i], y[i]))

        #-- use Thule for GLT
        if "Thule" in self.data: self.data["GLT"] = self.data["Thule"]

        self.t0 = self.data[obs[0]][0, 0]


#-- setup parser
parser = argparse.ArgumentParser(description='')
parser.add_argument("-f", "--config", required=True, help='Configuration file')
parser.add_argument("--server", default='http://localhost:8000', help='username for server authentication')
parser.add_argument("--username", default='admin', help='username for server authentication')
#-- parse arguments
args = parser.parse_args()


#-- config file
with open(args.config, "r") as f:
    conf = yaml.load(f)
    #-- sanity check
    for p in ["model_toffset", "model_linecolor", "xlim"]:
        assert isinstance(conf["TauPlots"][p], list)
        assert len(conf["TauPlots"][p]) > 0
    for p in ["width", "height", "dpi", "width2", "height2", "dpi2"]:
        assert isinstance(conf["TauPlots"][p], int)

width = conf["TauPlots"]["width"]/float(conf["TauPlots"]["dpi"])
height = conf["TauPlots"]["height"]/float(conf["TauPlots"]["dpi"])
dpi = conf["TauPlots"]["dpi"]
width2 = conf["TauPlots"]["width2"]/float(conf["TauPlots"]["dpi2"])
height2 = conf["TauPlots"]["height2"]/float(conf["TauPlots"]["dpi2"])
dpi2 = conf["TauPlots"]["dpi2"]

#-- target model dates and line colors
model_toffset = conf["TauPlots"]["model_toffset"]
model_linecolor = conf["TauPlots"]["model_linecolor"]
assert len(model_toffset) <= len(model_linecolor)


#-- time window
nowtime = datetime.datetime.utcnow()
dtime = max(model_toffset)
endtime = nowtime + datetime.timedelta(dtime)
# endtime = datetime(2017,02,19,18,39,43)     # override for testing purposes
starttime = nowtime - datetime.timedelta(dtime)     # 2 days before
datetime2timestamp = lambda dt: calendar.timegm(dt.timetuple())
xmin = md.epoch2num(datetime2timestamp(starttime))
xnow = md.epoch2num(datetime2timestamp(nowtime))

#-- model files
path = os.path.expandvars(conf["WeatherModelDirectory"])
fnames = sorted(glob.iglob(path + '/BHC_wind_20????????.dat'))

#-- model dates
mdates = []
for fname in fnames:
    fdate = fname[-14:-4]
    ftime = int(datetime.datetime.strptime(fdate, "%Y%m%d%H").strftime("%s"))
    mdates.append(ftime)
mdates = np.array(mdates)

#-- select model dates
idx = []
tmdates = mdates[-1] - np.array(model_toffset)*86400
for tmdate in tmdates:
    i = np.argmin(np.abs(mdates - tmdate))
    if len(idx) > 0  and  i == idx[-1]: continue
    idx.append(i)
fnames = [fnames[i] for i in idx]

#-- read selected model files
models = []
for i, toff in enumerate(model_toffset):
    fname = fnames[i]
    marsFile = MarsFile(fname)
    marsFile.color = model_linecolor[i]
    if toff == 0:
        label = "current"
    else:
        label = "-{}d".format(toff)
    marsFile.label = label
    models.append(marsFile)


# pprint(conf)

#-- local storage directory
storagedir = os.path.join(conf["WebDirectory"],"img/plots/")
# this expand (among other things) ${HOME}
storagedir = os.path.expandvars(storagedir)

#-- create storage dir if not exists
try:
    os.mkdir(storagedir)
    print "creating directory: " + storagedir
except OSError:
    pass

#-- fetch tau data
datafield = "weather_windSpeed"
obs = "&observatory=".join(conf["Observatories"])
urlquery = "/data/history?observatory={:}&field={:}&startTime={:}&endTime={:}".format(obs, datafield, datetime2timestamp(starttime), datetime2timestamp(endtime))

args.server.replace('localhost:8000', 'localhost%s'%conf["ServerAddress"])
print "server:", args.server
if not "localhost" in args.server:
    password = getpass.getpass("Password for {:}:".format(args.username))
    postdata["auth"] = {
        "facility": "RU",
        "username": args.username,
        "password": password
    }

r = requests.get(args.server + urlquery)
#pprint(r.json())

odata = {}
for obs, obsdata in r.json().iteritems():
    data = np.array(obsdata[datafield], dtype='float64')
    if data.shape[0] <= 1:
        #-- fake two data points to enable gap detection
        data = np.array([[xmin, xnow],[0, 0]])
    else:
        data = data.transpose()
        data[0] = md.epoch2num(data[0])
    odata[obs] = data[:]

#-- still missing data
for obs in conf["Observatories"]:
    if obs in odata:
        continue
    odata[obs] = np.array([[xmin, xnow],[0, 0]])

#-- mask gaps
odata_gaps = {}
for obs in odata.keys():
    t = np.ndarray([len(odata[obs][0]) + 2])
    t[1:-1] = odata[obs][0]
    t[[0, -1]] = [xmin, xnow]
    dt = np.diff(t)
    dtm = np.median(dt)
    #-- more than 10x median away
    idx = np.nonzero(dt > 10*dtm)[0]
    #-- save gap boundaries
    odata_gaps[obs] = np.vstack((t[idx], t[idx + 1])).transpose()
    #-- apply mask
    odata[obs] = np.ma.array(odata[obs])
    odata[obs][0, idx-1] = np.ma.masked

ymax = 20

#-- generate plots
for obs, data in odata.iteritems():
    print "processing", obs

    fig = plt.figure()

    ax = fig.add_subplot(111)
    #-- plot data
    #ax.plot(data[0], data[1], linewidth=1)

    #-- plot gaps
    for tup in odata_gaps[obs]:
        ax.axvspan(*tup, color="#ffcccc", zorder=0)

    #-- plot model
    for m in models:
        if not obs in m.data:
            print "model missing:", obs, m.label
            continue
        x = md.epoch2num(m.data[obs][0])
        y = m.data[obs][1]
        ax.plot(x, y, linewidth=1, color=m.color)

    ax.set_xmargin(0)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ax.tick_params(axis='y', which="major", direction="in",
        left=True, reset=False, labelleft=True, pad=2)

    ax.fill_between(data[0], 0, data[1], linewidth=0)
    ax.set_axisbelow(True)
    ax.xaxis_date(tz='UTC')

    ax.grid(color=".8")

    ax.set_ylim([0, ymax])

    ax.axvline(xnow, linewidth=1, color="k")


    for dtime in conf["TauPlots"]["xlim"]:
        locator = md.AutoDateLocator(minticks=6, maxticks=11)
        ax.xaxis.set_major_locator(locator)
        if dtime > 2:
            ax.xaxis.set_major_formatter(md.DateFormatter('%d'))
        else:
            ax.xaxis.set_major_formatter(md.DateFormatter('%d-%Hh'))

        xmin = xnow - dtime
        xmax = xnow + dtime
        ax.set_xlim([xmin, xmax])

        text = range(3)
        text[0] = ax.text(xnow - .01*(xmax - xmin), .85*ymax, "PAST", color='k', ha='right')
        text[1] = ax.text(xnow + .01*(xmax - xmin), .85*ymax, "FUTURE", color='k', ha='left')
        text[2] = ax.text(xmin + .95*(xmax - xmin), .85*ymax, "wind", color='k', ha='right', bbox={'ec': 'k', 'fc': 'w'})

        y = .85*ymax
        xleft = xmin + .01*(xmax-xmin)
        xright = xnow - .14*(xmax-xmin)
        for m in models[::-1]:
            x = md.epoch2num(m.t0)
            x = max(xleft, x)
            if x > xright: y = min(y, .7*ymax)
            t = ax.text(x, y, m.label, color=m.color, ha='left')
            if x > xright: y = .85*ymax #-- reset
            if x == xleft: y -= .15*ymax
            text.append(t)

        imgfile = os.path.join(storagedir, "windSpeed.{:}.d{:d}.png".format(obs, dtime))
        print "saving:", imgfile
        ax.set_position((0.06, 0.15, 1-0.06, 0.82)) #,frameon=False
        fig.set_size_inches(width, height)
        fig.savefig(imgfile, pad_inches=0, dpi=dpi, frameon=False)

        imgfile = os.path.join(storagedir, "windSpeed.{:}.d{:d}.wide.png".format(obs, dtime))
        print "saving:", imgfile
        ax.set_position((0.02, 0.15, 1-0.02, 0.82)) #,frameon=False
        fig.set_size_inches(width2, height2)
        fig.savefig(imgfile, pad_inches=0, dpi=dpi2, frameon=False)


        [t.remove() for t in text]
        del text

    plt.close(fig)
