#!/usr/bin/python
from __future__ import print_function
import requests
import datetime

server = 'vlbimon1'
SERVER = 'https://%s.science.ru.nl' % server
FNAME = '.sessionid.%s.txt' % server

def r2dbe_alt_sn(mac):
    D = 'D'
    for i in range(4):
        b = mac[4+2*i:4+2*i+2]
        D += '%02d' % (int(b,16))
    return D

def get_session():
    '''Setup user session with server'''
    def restore_session(s):
        #-- read from file
        with open(FNAME, 'r') as f:
            sessionid = f.readline().strip()
        #-- validate
        s.cookies.set('sessionid', sessionid)
        url = SERVER + '/session/' + sessionid
        r = s.patch(url)
        r.raise_for_status()
        return s
        
    def create_session(s):
        url = SERVER + '/session'
        import getpass
        username = input("Username:")
        password = getpass.getpass("Password for {:}:".format(username))
        r = s.post(url, auth=requests.auth.HTTPBasicAuth(username, password))
        r.raise_for_status()
        resp = r.json()
        sessionid = resp['id']
        s.cookies.set('sessionid', sessionid)
        #-- write to file
        with open(FNAME, 'w') as f:
            f.write(sessionid)
        return s

    s = requests.Session()
    #-- restore session
    try:
        return restore_session(s)
    except: pass
    #-- create new session if restore failed
    return create_session(s)

def fetch_timeseries(s, timebracket, sites=[], fields=[]):
    '''Fetch metadata timeseries data from server'''
    url = SERVER + '/data/history'

    tail = ['observatory=' + s for s in sites]
    tail += ['field=' + f for f in fields]
    tail += ['startTime=%d' % timebracket[0], 'endTime=%d' % timebracket[1]]
    if len(tail) > 0:
        url += '?' + '&'.join(tail)

    r = s.get(url)
    r.raise_for_status()
    return r.json()

if __name__ == "__main__":
    import numpy as np
    from scipy.stats import linregress
    import matplotlib.pyplot as plt
    plt.ion()

    ses = get_session()
    fields = ['r2dbe_{}_serialnumber'.format(x) for x in [1,2,3,4]]
    fields += ['recorder_{}_serialnumber'.format(x) for x in [1,2,3,4]]
    fields += ['bdc_{}_serialnumber'.format(x) for x in [1,2]]
    sites = ['ALMA','APEX','GLT','JCMT','KP','LMT','NOEMA','PICO','SMA','SMTO','SPT']
    timebracket = [t.timestamp() for t in [datetime.datetime(2022,1,1), datetime.datetime.utcnow()]]
    result = fetch_timeseries(ses, timebracket, sites, fields)

    for site,v1 in list(result.items()):
        print(site)
        for dev,v2 in list(v1.items()):
            host = ''.join(dev.split('_')[:2])
            for entry in v2[::-1]:
                sn = entry[1]
                if 'r2dbe' in dev: sn += ' (%s)' % (r2dbe_alt_sn(sn))
                lastupdate = entry[0]
                print('  {:16} : {:32} [{}]'.format(host,sn,datetime.datetime.fromtimestamp(lastupdate).strftime('%Y-%m-%d %H:%M:%S')))
                break
